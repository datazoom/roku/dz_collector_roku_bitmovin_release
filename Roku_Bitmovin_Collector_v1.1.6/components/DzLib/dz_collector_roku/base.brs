sub init()
    m.device = CreateObject("roDeviceInfo")
    m.task = createObject("roSGNode", "Task_CallAPI")
    m.sendApiTask = createObject("roSGNode", "Task_SendAPI")
    m.geoTask = createObject("roSGNode", "Task_GeoData")
    m.task.observeField("geoResult","onGeoResponseReceived")
    m.task.observeField("serverTimeResult","onServerTimeReceived")
    m.date = CreateObject("roDateTime")
    m.connectionTimer = m.top.findNode("connectionTimer")
    m.connectionTimer.ObserveField("fire","restartConnection")
    m.connectionTimer.control = "start"
    m.timerCount = 0
    m.socketInitiated = false
    m.setViewIDFlag = true
    m.appInfo = CreateObject("roAppInfo")
    m.videoCntKeyFlag = false
    m.geoData = {
                    city:"",
                    country: "",
                    countryCode : "",
                    lat: 00.00,
                    lon: 00.00,
                    regionCode: "",
                    asn : "",
                    asnOrganization: "",
                    client_ip: "",
                    isp: "",
                    region: "",
                    zip: ""
                }
    onGeoResponseReceived()
    m.dzBaseRegister = CreateObject("roRegistrySection","sessionIdReg")
    m.playerReadyState = 0
    m.noOfVid = 1
    m.videoURL = ""
    m.vSessionID = {}
    uniqueName = m.appInfo.GetID()+"-"+playerType()
    m.vewSessionIdKey= uniqueName + "-sessionViewId"
    m.noOfVideosKeyName = uniqueName + "-noOfVideos"
    m.sessionIdKey = uniqueName + "-sessionId"
    m.sessionIdTimeKey = uniqueName + "-sessionIdTime"
    m.sendTimeStamp = getCurrentTimestampInMillis()
    m.longPauseFlag = false
    m.retrieveTimeStamp = 0
    m.eventConfirmed = true
    m.apiReceivedTime = 0
    m.syncApiDelay = 0
    m.clientTimeOffset = 0
    m.clientTime = 0
    m.apiTimer = CreateObject("roTimespan")
    m.syncTimer = CreateObject("roTimespan")
    m.brokerDelay = 0
    m.pausedTime = 0
    m.viewIDSet = false
    m.dzBaseRegister.Delete(m.sessionIdKey)
    m.dzBaseRegister.Delete(m.vewSessionIdKey)
    m.dzBaseRegister.Flush()
    m.serverTimeMillis = 0    
    onServerTimeReceived()
end sub

' Method to initiate the collector library.
Sub initiateCollector()
    If m.top.initiateCollector
        m.eventConfig = getLibConfig().events
        If m.top.libConfiguration <> invalid
            ' playerType() must be available in collector brs file
            uniqueName= (m.top.libConfiguration.configId)+m.appInfo.GetID()+"-"+playerType()
            m.noOfVideosKeyName = uniqueName + "-noOfVideos"
            m.vewSessionIdKey= uniqueName + "-sessionViewId"
            m.sessionIdKey = uniqueName + "-sessionId"
            m.sessionIdTimeKey = uniqueName+"-sessionIdTime"
            m.videoCntKeyFlag = true
            getCustomerConfiguration()
        Else
           ? "FAILED TO INITIALISE DATAZOOM SDK"
        End If
    End If
End Sub

'Method to initiate the player
Sub initiatePlayer()
    if m.top.initiatePlayer
    ? "DATAZOOM ROKU BITMOVIN V3 PLAYER SDK INITIALIZED"
    m.playerStates = m.top.playerInit.player
    m.playerStates.observeField("state","playerStateBase")
    m.eventConfig = getLibConfig().events
    configureEvents()
'    m.videoCntKeyFlag = true
    End if
End Sub   

' Method to get the customer configuration from beacon-service.
Sub getCustomerConfiguration()
    ? "CUSTOMER CONFIG UPDATED"
'    m.playerStates = m.top.playerInit.player
'    m.playerStates.observeField("state","playerStateBase")
    configId= m.top.libConfiguration.configId
    configURL= m.top.libConfiguration.configURL
    requestData = {}
    requestData.httpMethodString = "GET"
    url = configURL.toStr() + "/beacon/v1/config?configuration_id=" +configId.toStr()
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    m.task.setField("requestData", requestData)
    m.task.observeField("result","onResponseReceived")
    m.task.control = "RUN"
    m.clientTime = getCurrentTimestampInMillis()
    m.syncTimer.Mark()
End Sub

' Player operations
Sub playerStateBase()
    if m.playerStates.state = "playing"
        m.playerReadyState = 1
'        if m.viewIDSet = false
'        callToSetNoOfVideos()
'        end if
    else if m.playerStates.state = "stopped"
        m.setViewIDFlag = true
    else if m.playerStates.state = "finished"
        m.setViewIDFlag = true
    else if m.playerStates.state = "error"
        m.setViewIDFlag = true
    end if
end sub

' Calls function setSessionViewId
function callToSetNoOfVideos()
    if m.setViewIDFlag
        m.setViewIDFlag = false
        setSessionViewId()
    end if
end function

' After API for customer configuration received.
Sub onResponseReceived()
    m.brokerDelay = m.apiTimer.TotalMilliseconds()
    if m.task.result <> invalid and m.task.result.bodystring <> invalid
        m.syncApiDelay =  m.syncTimer.TotalMilliseconds()
        m.responseBody = parseJSON(m.task.result.bodystring)
        if m.responseBody<> invalid and m.responseBody.data_collector <> invalid and m.responseBody.data_collector.url <> invalid
        '-----------Data point validation----------------
        m.top.connectionSuccess = true
        m.timerCount = 0
        ' Calling function in collector Library
        dataPointValidation()
'        retrieveEventData()
        '------------------------------------------------
            if checkSessionData() <> true
                setSessionData()
            end if
            m.top.events = m.responseBody.events
            
            If checkIfMetaConfigured(m.eventConfig.METASESSIONSTARTTIMESTAMP)
            m.template.user_details["app_session_start_ts_ms"] = getCurrentTimestampInMillis()
            end if
            
            m.serverTimeMillis = m.serverTimeResponseBody.epoch_millis
            m.timeDiff = m.serverTimeMillis - m.clientTime
            if m.brokerDelay <> 0 
            m.clientTimeOffset = m.serverTimeMillis - m.clientTime - (m.brokerDelay/2)
            else
            m.clientTimeOffset = m.serverTimeMillis - m.clientTime - (m.syncApiDelay/2)
            end if
            m.template.ops_metadata["server_ts_millis_offset"] = m.clientTimeOffset
            
            if checkIfEventConfigured(m.eventConfig.EVENTDATAZOOMLOADED)
           m.template.event.metrics = {}
           m.template.event.metrics[m.eventConfig.METAEVENTCOUNT] = 1
            wsSend(getMessageTemplate(m.eventConfig.EVENTDATAZOOMLOADED))
            m.TSLOADED = getCurrentTimestampInMillis()
            End If
        end if
    end if
End Sub

' After API for customer configuration received.
Sub onGeoResponseReceived()
    if m.task.geoResult <> invalid and m.task.geoResult.bodystring <> invalid and m.task.geoResult.responsecode = 200
        m.geoResponseBody = parseJSON(m.task.geoResult.bodystring)
        if m.geoResponseBody<> invalid
             m.geoData.city= m.geoResponseBody.city
             m.geoData.countryCode = m.geoResponseBody.countryCode
             m.geoData.country = m.geoResponseBody.country
             m.geoData.lat = m.geoResponseBody.lat
             m.geoData.lon = m.geoResponseBody.lon
             m.geoData.regionCode = m.geoResponseBody.region
             m.geoData.asn = m.geoResponseBody.as
             m.geoData.asnOrganization = m.geoResponseBody.org
             m.geoData.client_ip= m.geoResponseBody.query
             m.geoData.isp = m.geoResponseBody.isp
             m.geoData.region = m.geoResponseBody.regionName
             m.geoData.zip = m.geoResponseBody.zip
        end if
    end if

'    if m.task.serverTimeResult <> invalid and m.task.serverTimeResult.bodystring <> invalid
'        m.syncApiDelay =  m.syncTimer.TotalMilliseconds()
'        m.serverTimeResponseBody = parseJSON(m.task.serverTimeResult.bodystring)
'        if m.serverTimeResponseBody<> invalid
'             m.serverTimeMillis = m.serverTimeResponseBody.epoch_millis
'             else
'             m.serverTimeMillis = 0
'        end if
'    end if
End Sub

' After response from Broker received
Sub onBrokerResponse()
    If m.brokerResponseFired = false
    if m.sendApiTask.result <> invalid
        if m.sendApiTask.result.bodystring <> invalid  and m.sendApiTask.result.bodystring <> ""
        m.brokerResponseBody = parseJSON(m.sendApiTask.result.bodystring)
        m.apiStatusCode = m.brokerResponseBody.status_code
        else
        m.apiStatusCode = 505
        end if
        if m.brokerResponseBody <> invalid or m.sendApiTask.result <> invalid
        if m.apiStatusCode >= 500 and m.apiStatusCode < 600 or m.apiStatusCode = 429
            m.brokerResponseFired = true
            m.eventConfirmed = false
            storeEventData(m.wsDataTmp,true)
            m.brokerResponseFired = true
            m.eventConfirmed = true
            else if m.apiStatusCode = 400

            m.eventConfirmed = false
            m.brokerResponseFired = true
            else

            m.eventConfirmed = true
            m.brokerResponseFired = true 
            retrieveEventData()
            end if
        else 
            m.brokerResponseFired = true
        end if
    else
    storeEventData(wsData,true)
    m.sendApiTask.result = invalid
    end if  
    end if    
End Sub

Sub onServerTimeReceived()
    if m.task.serverTimeResult <> invalid and m.task.serverTimeResult.bodystring <> invalid
        m.syncApiDelay =  m.syncTimer.TotalMilliseconds()
        if m.serverTimeResponseBody <> "" or m.serverTimeResponseBody <> invalid
        m.serverTimeResponseBody = parseJSON(m.task.serverTimeResult.bodystring)
        end if
        if m.serverTimeResponseBody<> invalid
             m.serverTimeMillis = m.serverTimeResponseBody.epoch_millis
             else
             m.serverTimeMillis = 0
        end if
    end if
End Sub

'Method to send data to REST API
function wsSend(wsData)
'    retrieveEventData()
    m.wsDataTmp = wsData
    buffered = false
    requestData = {}
    requestData.httpMethodString = "POST"
    url = m.responseBody.broker_url.url
    requestData.urlString = url
    requestData.headersAssociativeArray = {"Content-Type": "application/json"}
    requestData.postBodyString = formatJSON([wsData])

    if m.eventConfirmed = true
        if m.storedData <> invalid
            if m.storedData.Count() > 0 
                for each buff in m.storedData
                    if  buff["event_id"] = wsData["event_id"]
                        buffered = true
                    end if
                end for
            
                if buffered = false                 
                    m.storedData.Push(wsData)
                end if

                requestData.postBodyString = formatJSON(m.storedData)
                m.storedData = invalid
            
            else

                if isArray(wsData)
                   requestData.postBodyString = formatJSON(wsData)
               else
                   requestData.postBodyString = formatJSON([wsData])
               end if
                'requestData.postBodyString = formatJSON([wsData])

            end if
        end if
        m.sendApiTask.setField("requestData", requestData)
        m.sendApiTask.observeField("result","onBrokerResponse")
        m.sendApiTask.control = "RUN"
        m.brokerResponseFired = false
        m.eventConfirmed = false
     else  
            storeEventData(wsData,true)
    end if   
        
End Function

function isArray(array):
    return getInterface(array, "ifArray") <> invalid
end function

function storeEventData(wsData,flag)
inStore = false
if isArray(wsData)
? "DATA IS ARRAY"
else
    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        dataArray = parseJSON(m.dzBaseRegister.Read("eventData"))
        if dataArray = invalid
            dataArray = {}
        end if
        for each storedKey in dataArray
        if dataArray[storedKey]["event_id"] = wsData["event_id"]
        inStore = true
        end if
        end for
        if inStore = false
        
            if isArray(wsData)
                for each wsDat in wsData
                key= getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
                dataArray[key] = wsDat
            end for
            else
            key= getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
            dataArray[key]=wsData
            end if
        
        end if
        
        m.dzBaseRegister.Write("eventData", FormatJson(dataArray))
        if flag
'            print "Processed data in storage : Count = "  dataArray.Count()
        end if
    else
        dataArray = {}
        key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
        
        for each storedKey in dataArray
        if dataArray[storedKey] = wsData
        inStore = true
        end if
        end for
        
        if inStore = false
        dataArray[key]=wsData
        end if
        m.dzBaseRegister.Write("eventData", FormatJson(dataArray))
        if flag
'            print "Processed data in storage : Count = "  dataArray.Count()
        end if
    end if
    
end if
end function

' Method to retrieve the stored event data and
' send it to api when network is open. 5 push at a time is allowed.

function retrieveEventData()
    sendCount = 0
    jsonArray = CreateObject("roArray", 8, true)
    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        if m.dzBaseRegister.Read("eventData") <> ""
          
            dataArray = parseJSON(m.dzBaseRegister.Read("eventData"))
            m.dzBaseRegister.Delete("eventData")
        end if
        
        if dataArray.Count() > 0 
            for each key in dataArray
                jsonArray.push(dataArray[key])  
            end for
                m.storedData = jsonArray
        end if
    end if
end function

function purgeEventData()
    ? "PURGE BATCHED EVENTS"
    jsonArray = CreateObject("roArray", 8, true)
    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        if m.dzBaseRegister.Read("eventData") <> ""
            dataArray = parseJSON(m.dzBaseRegister.Read("eventData"))
            m.dzBaseRegister.Delete("eventData")
        end if  
        if dataArray.Count() > 0
            for each key in dataArray
                jsonArray.push((dataArray[key]))
            end for
            ? "PURGE "; (formatJSON(jsonArray))
'             wsSend(formatJSON(jsonArray))
'             jsonArray.Clear()
        end if
    end if
end function

' Function to call setSessionTime. This function will be called by both base and collector
function callSetSessionTime()
    setSessionTime()
end function

' Method to check if an event is been configured by customer.
Function checkIfEventConfigured(event as String) as boolean
    m.media_type = getMediaType()
    if m.top.events_v3 <> invalid
    for each eventV3 in m.top.events_v3
        If eventV3.name = event
            for each mType in eventV3.media_types
            if mType = m.media_type or mType = "na"
'               ? "TRIGGER EVENT V3:"; eventV3.name ;"="; event
'               ? "MEDIA TYPE FOR EVENT V3 = "; eventV3.media_types
                return true
                
            End If
            end for
        end if
    end for
  else
    If m.top.events <> invalid
        if m.top.events.types <> invalid
        For Each eventType in m.top.events.types
            If eventType.name = event
'               ? "TRIGGER EVENT V2:"; eventType.name ;"="; event
                return true
            End If
        end For
        end if
    End If
 end if
    return false
End Function

'Function to check if Fluxdata is available
Function checkIfFluxConfigured(event as String) as boolean   
    If m.top.events <> invalid 
        if m.top.events.flux_data <> invalid
        For Each fluxType in m.top.events.flux_data
            If fluxType = event
                return true
            End If
        end For
        end if
    End If
    return false
End Function

'Function to check if Metadata is available
Function checkIfMetaConfigured(event)  
    If m.top.events <> invalid   
        if m.top.events.metdata <> invalid
        For Each metaType in m.top.events.metdata
            If metaType = event
                return true
            End If
        end For
        end if
        
    End If
    return false
End Function

' Method to get unix epoch time stamp in millisecond 
Function getCurrentTimestampInMillis()
    dateObj = createObject("roDateTime")
    currentSeconds = dateObj.AsSeconds()
    m& = 1000
    currentMilliseconds = currentSeconds * m&
    ms = currentMilliseconds + dateObj.GetMilliseconds()
    return ms
End Function

Function getUniqueDeviceId()
    return createObject("roDeviceInfo").GetChannelClientId()
End Function

Function getAdId()
    return createObject("roDeviceInfo").GetRIDA()
End Function

' Returns device's version
Function getVersion()
    return createObject("roDeviceInfo").GetVersion()
end function

' Returns Client IP
Function getIpAddress()
    return m.geoData.client_ip
end function

' Returns Client city
Function getCity()
    return m.geoData.city
end function

' Returns Client ZIP
Function getZip()
    return m.geoData.zip
end function

' Returns Client country code
Function getCountryCode()
    return m.geoData.countryCode
end function

' Returns Client country
Function getCountry()
    return m.geoData.country
end function

' Returns Client latitude
Function getLatitude()
    return m.geoData.lat
end function

' Returns Client longitude
Function getLongitude()
    return m.geoData.lon
end function

' Returns Client region code
Function getRegionCode()
    return m.geoData.regionCode
end function

' Returns Client region
Function getRegion()
    return m.geoData.region
end function

Function getasn()
    return m.geoData.asn
end function

Function getasnOrg()
    return m.geoData.asnOrganization
end function

' Returns Client ISP
Function getISP()
    return m.geoData.isp
end function


' Returns device OS
Function getOS()
    return "Roku OS"
end function

' Returns device OS version
Function getOSVersion()
    ver = createObject("roDeviceInfo").GetOSVersion()
    version = ver.major + "."+ver.minor+"."+ver.revision+"."+ver.build
    return version
end function

' Returns device name
Function getModelDisplayName()
 return createObject("roDeviceInfo").GetModelDisplayName()
end function
' End

' Returns device type
Function getDeviceType()
return "ott device"
end Function
' End

' Returns video type
Function getVideoType()
return "Content"
end Function
' End

' Function to get random number
function randDigit(count)
    value = ""
    for i=1 to count
        value += Rnd(9).toStr()
    end for
    return value
end function

' Function to get session ID
Function getSessionData()
     sessionIdBool = 0
     if m.dzBaseRegister.Exists(m.sessionIdKey) and m.dzBaseRegister.Exists(m.sessionIdTimeKey)
         if(getCurrentTimestampInMillis() - Val(m.dzBaseRegister.Read(m.sessionIdTimeKey)) < 1260000)
            sessionIdBool = 1
            sessionId = m.dzBaseRegister.Read(m.sessionIdKey)
         end if
     end if
     if sessionIdBool = 0
        setSessionData()
     else
        return sessionId
     end if
End Function

'function to get sessionStartTimestamp
Function getSessionStartTimestamp()

End Function

'Function to check session ID
Function checkSessionData()
     sessionIdBool = 0
     if m.dzBaseRegister.Exists(m.sessionIdKey) and m.dzBaseRegister.Exists(m.sessionIdTimeKey)
         if(getCurrentTimestampInMillis() - Val(m.dzBaseRegister.Read(m.sessionIdTimeKey)) < 1260000)
            sessionIdBool = 0
         end if
     end if
     if sessionIdBool = 0
        return false
     else
        return true
     end if
End Function

'Function to set session ID
function setSessionData()
    if m.videoCntKeyFlag
        sessionId = m.device.GetRandomUUID()
        m.dzBaseRegister.Write(m.sessionIdKey, sessionId)
        m.numberOfAdsPlayed = 0
        m.dzBaseRegister.Write(m.sessionIdTimeKey, getCurrentTimestampInMillis().ToStr())
        if m.dzBaseRegister.Exists(m.vewSessionIdKey)
            m.dzBaseRegister.Delete(m.vewSessionIdKey)
        end if
        if m.dzBaseRegister.Exists(m.noOfVideosKeyName)
            m.dzBaseRegister.Delete(m.noOfVideosKeyName)
        end if
        viewID = m.device.GetRandomUUID()
        m.dzBaseRegister.Write(m.vewSessionIdKey, viewID.ToStr())
        m.setViewIDFlag = false
        return m.dzBaseRegister.Read(m.sessionIdKey)
     else 
        return m.dzBaseRegister.Read(m.sessionIdKey)' ""
     end if
end function

'Function to set session Time
function setSessionTime()
        if m.dzBaseRegister.Exists(m.sessionIdKey)
            m.dzBaseRegister.Write(m.sessionIdTimeKey, getCurrentTimestampInMillis().ToStr())
            m.dzBaseRegister.Flush()
            return true
        else
            return false
        end if
end function

'Function to session view ID
function setSessionViewId()
        
        if m.dzBaseRegister.Exists(m.vewSessionIdKey)
           videoCnt = Val(m.dzBaseRegister.Read(m.vewSessionIdKey))
        end if
        videoCnt= m.device.GetRandomUUID()
        m.dzBaseRegister.Write(m.vewSessionIdKey, videoCnt.ToStr())
end function

'Function to get session View ID
function getSessionViewId()
    if m.dzBaseRegister.Exists(m.vewSessionIdKey)
        return m.dzBaseRegister.Read(m.vewSessionIdKey)     
    else
        return ""
    end if
end function

'Function to get player ready state
function getPlayerReadyState()
return m.playerReadyState.toStr()
end function

'Function to set current video URL
function setContentUrlToBase(videoURLCOL)
    m.videoURL = videoURLCOL
'    setNoOfVideos()
'    m.viewIDSet = true
end function

'Function to set number of videos in current session
function setNoOfVideos()
    ba = CreateObject("roByteArray") 
    ba.FromAsciiString(m.videoURL)
    getUrl = ba.ToBase64String()
    if getUrl <> ""
        vURL = getUrl
        if m.dzBaseRegister.Exists(m.noOfVideosKeyName)
            nVideoArray = {}
            nVideoArray = parseJSON(m.dzBaseRegister.Read(m.noOfVideosKeyName))
            if nVideoArray[vURL] <> invalid
                m.noOfVid = nVideoArray.Count()
            else
                nVideoArray[vURL] = getCurrentTimestampInMillis().ToStr()
                m.dzBaseRegister.Write(m.noOfVideosKeyName, FormatJson(nVideoArray))
                m.noOfVid = nVideoArray.Count()
                setSessionViewId()
            end if
          else
                nVideoArray = {}
                nVideoArray[vURL] = getCurrentTimestampInMillis().ToStr()
                m.dzBaseRegister.Write(m.noOfVideosKeyName, FormatJson(nVideoArray))
                m.noOfVid = 1
        end if
    end if
end function

'Function to get number of video during current session
function getNoOfVideos()
    return m.noOfVid.toStr()
end function
function getConnectionType()
    return createObject("roDeviceInfo").GetConnectionType()
end function