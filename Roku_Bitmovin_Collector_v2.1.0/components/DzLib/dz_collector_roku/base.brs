sub init()
    m.device = CreateObject("roDeviceInfo")
    m.task = createObject("roSGNode", "Task_CallAPI")
    m.sendApiTask = createObject("roSGNode", "Task_SendAPI")
    m.sendSamplingTask = createObject("roSGNode", "Task_SendSampling")
    m.geoTask = createObject("roSGNode", "Task_GeoData")
    m.serverTimeTask = createObject("roSGNode", "Task_GetServerTime")
'    m.task.observeField("geoResult","onGeoResponseReceived")
'    m.geoTask.observeField("geoResult","onGeoResponseReceived")
'    m.task.observeField("serverTimeResult","onServerTimeReceived")
    m.date = CreateObject("roDateTime")
    m.connectionTimer = m.top.findNode("connectionTimer")
    m.connectionTimer.control = "start"
    m.apiBusTimer = m.top.findNode("apiBusTimer")
    m.apiBusTimer.control = "start"
    m.apiBusTimer.ObserveField("fire","apiBus")
    m.timerCount = 0
    m.setViewIDFlag = true
    m.appInfo = CreateObject("roAppInfo")
    m.videoCntKeyFlag = false
    m.geoData = {
                    city:"",
                    country: "",
                    countryCode : "",
                    lat: "",
                    lon: "",
                    regionCode: "",
                    asn : "",
                    asnOrganization: "",
                    client_ip: "",
                    isp: "",
                    region: "",
                    zip: "",
                    timezone:"",
                    timezone_offset:""
                }
'    onGeoResponseReceived()
    m.dzBaseRegister = CreateObject("roRegistrySection","sessionIdReg")
    m.playerReadyState = 0
    m.noOfVid = 1
    m.videoURL = ""
    m.vSessionID = {}
    uniqueName = m.appInfo.GetID()+"-"+playerType()
    m.vewSessionIdKey= uniqueName + "-sessionViewId"
    m.noOfVideosKeyName = uniqueName + "-noOfVideos"
    m.sessionIdKey = uniqueName + "-sessionId"
    m.sessionIdTimeKey = uniqueName + "-sessionIdTime"
    m.sendTimeStamp = getCurrentTimestampInMillis()
    m.longPauseFlag = false
    m.retrieveTimeStamp = 0
    m.eventConfirmed = true
    m.apiReceivedTime = 0
    m.syncApiDelay = 0
    m.clientTimeOffset = 0
    m.clientTime = 0
    m.apiTimer = CreateObject("roTimespan")
    m.syncTimer = CreateObject("roTimespan")
    m.brokerDelay = 0
    m.pausedTime = 0
    m.viewIDSet = false
    m.dzBaseRegister.Delete(m.sessionIdKey)
    m.dzBaseRegister.Delete(m.vewSessionIdKey)
    m.dzBaseRegister.Flush()
    m.serverTimeMillis = 0   
    m.media_type = "content" 
'    onServerTimeReceived()
    m.sessionStartTimestamp = 0
    m.dataBuffer = CreateObject("roList")
    m.brokerResponseFired = false
    m.delayEventTimer = CreateObject("roTimespan")
    m.storedData = CreateObject("roArray", 5, true)
    dim wsDatTmp [10]
    m.wsDataTmp = wsDatTmp
    dim evntJson [10]
    m.eventJson = evntJson
    m.geoCheck = ["client_ip",
                "country",
                "country_code",
                "region_code",
                "region",
                "city",
                "district",
                "postal_code",
                "latitude",
                "longitude",
                "timezone_name",
                "timezone_offset",
                "isp",
                "asn_org",
                "asn",
                "mobile_connection",
                "continent",
                "continent_code",
                "district"]
     m.getGeo = false
     m.serverTimeUrl = "https://broker.datazoom.io/broker/v1/getEpochMillis"
     m.pendingForSampling = true
     m.samplingURL = ""
     m.samplingOverride = false
     m.samplingTemplate = {}
     m.samplingTemplate.sampling_definition_params = {}
     m.samplingTemplate.sampling_criteria_params = {}
     m.samplingTemplate.sampling_criteria_params.sampling = {}
     m.samplingTemplate.matching = {}
     m.samplingConfirmed = true
end sub

' Method to initiate the collector library.
Sub initiateCollector()
    If m.top.initiateCollector
'        ResetAll()
        getCustomerConfiguration()
        m.sessionStartTimestamp = getCurrentTimestampInMillis()
        m.eventConfig = getLibConfig().events
        m.media_type = getMediaType()
        If m.top.libConfiguration <> invalid
            ? "DATAZOOM ROKU BASE (BITMOVIN) V3 SDK INITIALIZED"
            ' playerType() must be available in collector brs file
            uniqueName= (m.top.libConfiguration.configId)+m.appInfo.GetID()+"-"+playerType()
            m.noOfVideosKeyName = uniqueName + "-noOfVideos"
            m.vewSessionIdKey= uniqueName + "-sessionViewId"
            m.sessionIdKey = uniqueName + "-sessionId"
            m.sessionIdTimeKey = uniqueName+"-sessionIdTime"
            m.videoCntKeyFlag = true
'            getCustomerConfiguration()
        Else
        ? "FAILED TO INITIALISE DATAZOOM SDK"
        End If
    End If
End Sub

'Method to initiate the player
Sub initiatePlayer()
    if m.top.initiatePlayer
    ? "DATAZOOM ROKU BITMOVIN V3 SDK PLAYER INITIALIZED"
    m.playerStates = m.top.playerInit.player
    m.playerStates.observeField("state","playerStateBase")
    m.eventConfig = getLibConfig().events
    configureEvents()
    End if
End Sub   

' Method to get the customer configuration from beacon-service.
Sub getCustomerConfiguration()
    m.template.event.metrics = {}
    m.apiTimer.Mark()
    configId= m.top.libConfiguration.configId
    configURL= m.top.libConfiguration.configURL
    requestData = {}
    requestData.httpMethodString = "GET"
    url = configURL.toStr() + "/beacon/v1/config?configuration_id=" +configId.toStr()
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    m.task.setField("requestData", requestData)
    m.task.observeField("result","onResponseReceived")
    m.task.control = "RUN"
End Sub

' Player operations
Sub playerStateBase()
    if m.playerStates.state = "playing"
        m.playerReadyState = 1
    else if m.playerStates.state = "stopped"
        m.setViewIDFlag = true
    else if m.playerStates.state = "finished"
        m.setViewIDFlag = true
    else if m.playerStates.state = "error"
        m.setViewIDFlag = true
    end if
end sub

' Calls function setSessionViewId
function callToSetNoOfVideos()
    if m.setViewIDFlag
        m.setViewIDFlag = false
    end if
end function

' After API for customer configuration received.
Sub onResponseReceived()
    m.brokerDelay = m.apiTimer.TotalMilliseconds()
    'Get collector config
    if m.task.result <> invalid and m.task.result.bodystring <> invalid
        m.responseBody = parseJSON(m.task.result.bodystring)
        if m.responseBody<> invalid and m.responseBody.broker_url <> invalid and m.responseBody.broker_url.url <> invalid
        '-----------Data point validation----------------
        m.top.connectionSuccess = true
        m.timerCount = 0
        ' Calling function in collector Library
        dataPointValidation()
'        retrieveEventData()
        '------------------------------------------------
            if checkSessionData() <> true
                setSessionData()
                m.appSessionID = setSessionData()
            end if
            m.template.user_details["app_session_start_ts_ms"] = getCurrentTimestampInMillis()
             
            m.top.events = m.responseBody.events
            m.top.events_v3 = m.responseBody.events_v3
            m.serverTimeUrl = m.responseBody.epoch_url
            
            m.top.adMilestones = m.responseBody.milestones_ad
            m.top.contentMilestones = m.responseBody.milestones_content
            
            m.top.adQV = m.responseBody.qv_ad
            m.top.contentQV = m.responseBody.qv_content
            
            getServerTime(m.serverTimeUrl)
            if m.responseBody.sampling.is_sampling = "true"
                m.sampleIn = true
                m.pendingForSampling = false
                m.samplingURL = m.responseBody.sampling.sampling_api
                m.samplingDefNameSpace = m.responseBody.sampling.sampling_def_namespace
                m.samplingDefKeyName = m.responseBody.sampling.sampling_def_key_name
                m.samplingKeyName = m.responseBody.sampling.sampling_key_name
                m.samplingKeyValue = "TEST"
                m.samplingTemplate.sampling_request_id = m.device.GetRandomUUID()
                m.samplingTemplate.sampling_override = "NONE"
                m.samplingTemplate.sampling_definition_params.namespace = m.samplingDefNameSpace
                m.samplingTemplate.sampling_definition_params.key_name = m.samplingDefKeyName
                m.samplingTemplate.sampling_definition_params.key_value = m.responseBody.configuration_id
                m.samplingTemplate.sampling_criteria_params.sampling.key_name = m.samplingKeyName
                m.samplingTemplate.sampling_criteria_params.sampling.key_value = m.appSessionId
                sendToSampling(m.samplingTemplate)
                else
                m.sampleIn = false
                end if
            'check if geo IP lookup is needed
            For Each metaType in m.top.events.metdata
                for each geoChk in m.geoCheck
                if metaType = geoChk
                m.getGeo = true
                Exit for
                end if                
                end for
                if m.getGeo = true
                getGeoData()
                Exit for
                end if
            end for
        end if
    end if

End Sub

Function ClientSideSampling()

end Function

function sendToSampling(samplingData)
    m.samplingDataTmp = samplingData
    buffered = false
    requestData = {}
    requestData.httpMethodString = "POST"
    requestData.urlString = m.samplingURL
    requestData.headersAssociativeArray = {"Content-Type": "application/json", "cache-control": "no-cache"}
    requestData.postBodyString = formatJSON(samplingData)

    if m.samplingConfirmed = true
        if m.storedSamplingData <> invalid
            if m.storedSamplingData.Count() > 0 
                for each buff in m.storedSamplingData
                    if  buff["sampling_request_id"] = wsData["sampling_request_id"]
                        buffered = true
                    end if
                end for
            
                if buffered = false                 
                    m.storedSamplingData.Push(samplingData)
                end if

                requestData.postBodyString = formatJSON(m.storedSamplingData)
                m.storedData = invalid
            
            else
                
                if isArray(samplingData)
                   requestData.postBodyString = formatJSON(samplingData)
               else
                   requestData.postBodyString = formatJSON([samplingData])
               end if

            end if
        end if
        m.sendSamplingTask.setField("requestData", requestData)
        m.sendSamplingTask.observeField("result","onSamplingResponse")
        m.sendSamplingTask.control = "RUN"
        m.samplingResponseFired = false
        m.samplingConfirmed = false
     else  
            storeSamplingData(samplingData,true)
    end if           
End Function

sub onSamplingResponse()
 If m.samplingResponseFired = false
    if m.sendSamplingTask.result <> invalid
        
        if m.sendSamplingTask.result.responsecode <> invalid
        m.samplingStatusCode = m.sendSamplingTask.result.responsecode
        else
        m.samplingStatusCode = 505
        end if
        if m.sendSamplingTask.result <> invalid
        if m.samplingStatusCode >= 500 and m.samplingStatusCode < 600 or m.samplingStatusCode = 429
            m.samplingResponseFired = true
            m.samplingConfirmed = false
            storeSamplingData(m.samplingDataTmp,true)
            m.samplingResponseFired = true
            m.samplingConfirmed = true
            else if m.apiStatusCode = 400

            m.samplingConfirmed = false
            m.samplingResponseFired = true
            else

            m.samplingConfirmed = true
            m.samplingResponseFired = true 
            retrieveSamplingData()
            end if
        else 
            m.samplingResponseFired = true
        end if
    else
    storeSamplingData(samplingData,true)
    m.sendSamplingTask.result = invalid
    end if  
    end if    
end sub

function retrieveSamplingData()
    sendCount = 0
    jsonArray = CreateObject("roArray", 8, true)
    if m.dzBaseRegister.Exists("samplingData")
        dataArray = {}
        if m.dzBaseRegister.Read("samplingData") <> ""
          
            dataArray = parseJSON(m.dzBaseRegister.Read("samplingData"))
            m.dzBaseRegister.Delete("samplingData")
        end if
        
        if dataArray.Count() > 0 
            for each key in dataArray
                jsonArray.push(dataArray[key])  
            end for
                m.storedSamplingData = jsonArray
        end if
    end if
end function

function storeSamplingData(samplingData,flag)
inStore = false
if isArray(samplingData)
else
    if m.dzBaseRegister.Exists("samplingData")
        dataArray = {}
        dataArray = parseJSON(m.dzBaseRegister.Read("samplingData"))
        if dataArray = invalid
            dataArray = {}
        end if
        for each storedKey in dataArray
        if dataArray[storedKey]["sampling_request_id"] = samplingData["sampling_request_id"]
        inStore = true
        end if
        end for
        if inStore = false
        
            if isArray(samplingData)
                for each samDat in samplingData
                key= getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
                dataArray[key] = samDay
            end for
            else
            key= getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
            dataArray[key]=samplingData
            end if
        
        end if
        
        m.dzBaseRegister.Write("samplingData", FormatJson(dataArray))
        if flag
        end if
    else
        dataArray = {}
        key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
        
        for each storedKey in dataArray
        if dataArray[storedKey] = samplingData
        inStore = true
        end if
        end for
        
        if inStore = false
        dataArray[key]=samplingData
        end if
        m.dzBaseRegister.Write("samplingData", FormatJson(dataArray))
        if flag
'            print "Processed data in storage : Count = "  dataArray.Count()
        end if
    end if  
end if
end function



Function getGeoData()
    requestData = {}
    requestData.httpMethodString = "GET"
    url = "https://pro.ip-api.com/json/?key=ZP6KtPdtCCRcgGk&fields=49868799"'33615871"
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    m.geoTask.setField("requestData", requestData)
    m.geoTask.observeField("result","onGeoResponseReceived")
    m.geoTask.control = "RUN"   
end function

function getServerTime(serverTimeUrl)
    m.clientTime = getCurrentTimestampInMillis()
    m.syncTimer.Mark()
    requestData = {}
    requestData.httpMethodString = "GET"
    if serverTimeUrl <> invalid
    url = serverTimeUrl
    else
    url = "https://broker.datazoom.io/broker/v1/getEpochMillis"
    end if
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    m.serverTimeTask.setField("requestData", requestData)
    m.serverTimeTask.observeField("result","onServerTimeReceived")
    m.serverTimeTask.control = "RUN"   
end function

' After API for customer configuration received.
Sub onGeoResponseReceived()
    if m.geoTask.result <> invalid and m.geoTask.result.bodystring <> invalid and m.geoTask.result.responsecode = 200
        m.geoResponseBody = parseJSON(m.geoTask.result.bodystring)
        if m.geoResponseBody<> invalid
             m.geoData.city= m.geoResponseBody.city
             m.geoData.countryCode = m.geoResponseBody.countryCode
             m.geoData.country = m.geoResponseBody.country
             m.geoData.lat = m.geoResponseBody.lat
             m.geoData.lon = m.geoResponseBody.lon
             m.geoData.regionCode = m.geoResponseBody.region
             m.geoData.asn = m.geoResponseBody.as
             m.geoData.asnOrganization = m.geoResponseBody.org
             m.geoData.client_ip= m.geoResponseBody.query
             m.geoData.isp = m.geoResponseBody.isp
             m.geoData.region = m.geoResponseBody.regionName
             m.geoData.zip = m.geoResponseBody.zip
             m.geoData.timezone = m.geoResponseBody.timezone
             m.geoData.timezone_offset = m.geoResponseBody.offset
             m.geoData.continent = m.geoResponseBody.continent
             m.geoData.continentCode = m.geoResponseBody.continentCode
             m.geoData.district = m.geoResponseBody.district
        end if
    end if
    if checkIfEventConfigured(m.eventConfig.EVENTDATAZOOMLOADED)
           m.template.event.metrics = {}
           m.template.event.metrics[m.eventConfig.METAEVENTCOUNT] = 1
            wsSend(getMessageTemplate(m.eventConfig.EVENTDATAZOOMLOADED))
            m.TSLOADED = getCurrentTimestampInMillis()
    End If
    m.top.connectionSuccess = true
End Sub

'NEW METHOD
Sub onBrokerResponse()
    If m.brokerResponseFired = false
    if m.sendApiTask.result <> invalid
        if m.sendApiTask.result.bodystring <> invalid  and m.sendApiTask.result.bodystring <> ""
        m.brokerResponseBody = parseJSON(m.sendApiTask.result.bodystring)
        m.apiStatusCode = m.brokerResponseBody.status_code
        else
        m.apiStatusCode = 505
        end if
        if m.brokerResponseBody <> invalid or m.sendApiTask.result <> invalid
        if m.apiStatusCode >= 500 and m.apiStatusCode < 600 or m.apiStatusCode = 429
            m.brokerResponseFired = true
            m.eventConfirmed = false
            storeEventData(m.wsDataTmp,true)
            m.brokerResponseFired = true
            m.eventConfirmed = true
            else if m.apiStatusCode = 400

            m.eventConfirmed = false
            m.brokerResponseFired = true
            else

            m.eventConfirmed = true
            m.brokerResponseFired = true 
'            retrieveEventData()
            end if
        else 
            m.brokerResponseFired = true
        end if
    else
    storeEventData(wsData,true)
    m.sendApiTask.result = invalid
    end if  
    end if    
End Sub


Sub onServerTimeReceived()
    m.syncApiDelay =  m.syncTimer.TotalMilliseconds()
    if m.serverTimeTask.result <> invalid and m.serverTimeTask.result.bodystring <> invalid and m.serverTimeTask.result.responsecode = 200
        m.serverTimeResponseBody = parseJSON(m.serverTimeTask.result.bodystring)
        if m.serverTimeResponseBody<> invalid
             m.serverTimeMillis = m.serverTimeResponseBody.epoch_millis
             m.timeDiff = m.serverTimeMillis - m.clientTime
             else
             m.serverTimeMillis = m.clientTime
             m.timeDiff = m.serverTimeMillis - m.clientTime
        end if
    else
    m.serverTimeMillis = 0
    end if
    if m.serverTimeMillis <> invalid
       if m.brokerDelay <> 0 
            m.clientTimeOffset = m.serverTimeMillis - m.clientTime - (m.brokerDelay/2)
       else 
            m.clientTimeOffset = m.serverTimeMillis - m.clientTime - (m.syncApiDelay/2)
       end if
     m.template.ops_metadata["server_ts_millis_offset"] = m.clientTimeOffset
    else
     m.template.ops_metadata["server_ts_millis_offset"] = 0
    end if
    
'    if m.serverTimeTask.serverTimeResult <> invalid and m.serverTimeTask.serverTimeResult.bodystring <> invalid
'        m.syncApiDelay =  m.syncTimer.TotalMilliseconds()
'        if m.serverTimeResponseBody <> "" or m.serverTimeResponseBody <> invalid
'        m.serverTimeResponseBody = parseJSON(m.serverTimeTask.serverTimeResult.bodystring)
'        end if
'        
'    end if
End Sub

function apiBus()' 200ms departure
    if m.wsDataTmp.Count()> 0 and m.eventConfirmed = true
    ? "DZ-Print: API BUS JUST DEPARTED WITH "; m.wsDataTmp.Count(); " Events!"
    for each evnt in m.wsDataTmp
         m.eventJson.Push(parseJson(evnt))
    end for 
    m.sendApiTask = createObject("roSGNode", "Task_SendAPI")
    requestData = {}
    requestData.httpMethodString = "POST"
    url = m.responseBody.broker_url.url
    requestData.urlString = url
    requestData.headersAssociativeArray = {"Content-Type": "application/json"}
    requestData.postBodyString = formatJSON (m.eventJson)
    m.sendApiTask.setField("requestData", requestData)
    m.sendApiTask.observeField("result","onBrokerResponse")
    m.sendApiTask.control = "RUN"
    m.brokerResponseFired = false
    m.eventConfirmed = false
    m.wsDataTmp = []
    m.eventJson = []
    end if
end function

'Method to send data to REST API
'NEW METHOD
function wsSend(wsData)
          wsDataJson = formatJSON (wsData)
          m.wsDataTmp.Push(wsDataJson)
End Function

'function wsSend(wsData)
''    retrieveEventData()
'    m.wsDataTmp = wsData
'    buffered = false
'    requestData = {}
'    requestData.httpMethodString = "POST"
'    url = m.responseBody.broker_url.url
'    requestData.urlString = url
'    requestData.headersAssociativeArray = {"Content-Type": "application/json"}
'    requestData.postBodyString = formatJSON([wsData])
'
'    if m.eventConfirmed = true
'        if m.storedData <> invalid
'            if m.storedData.Count() > 0 
'                for each buff in m.storedData
'                    if  buff["event_id"] = wsData["event_id"]
'                        buffered = true
'                    end if
'                end for
'            
'                if buffered = false                 
'                    m.storedData.Push(wsData)
'                end if
'
'                requestData.postBodyString = formatJSON(m.storedData)
'                m.storedData = invalid
'            
'            else
'
'                if isArray(wsData)
'                   requestData.postBodyString = formatJSON(wsData)
'               else
'                   requestData.postBodyString = formatJSON([wsData])
'               end if
'                'requestData.postBodyString = formatJSON([wsData])
'
'            end if
'        end if
'        m.sendApiTask.setField("requestData", requestData)
'        m.sendApiTask.observeField("result","onBrokerResponse")
'        m.sendApiTask.control = "RUN"
'        m.brokerResponseFired = false
'        m.eventConfirmed = false
'     else  
'            storeEventData(wsData,true)
'    end if   
'        
'End Function


function isArray(array):
    return getInterface(array, "ifArray") <> invalid
end function

Function postToBroker(jsonMessage)
'REMOVED OLD FUNCTION
end function

' This method is used to store the event data when needed
function storeEventData(wsData,flag)
inStore = false
if isArray(wsData)
? "DATA IS ARRAY"
else
    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        dataArray = parseJSON(m.dzBaseRegister.Read("eventData"))
        if dataArray = invalid
            dataArray = {}
        end if
        for each storedKey in dataArray
        if dataArray[storedKey]["event_id"] = wsData["event_id"]
        inStore = true
        end if
        end for
        if inStore = false
        
            if isArray(wsData)
                for each wsDat in wsData
                key= getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
                dataArray[key] = wsDat
            end for
            else
            key= getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
            dataArray[key]=wsData
            end if
        
        end if
        
        m.dzBaseRegister.Write("eventData", FormatJson(dataArray))
        if flag
'            print "Processed data in storage : Count = "  dataArray.Count()
        end if
    else
        dataArray = {}
        key = getCurrentTimestampInMillis().toStr() + randDigit(3).toStr()
        
        for each storedKey in dataArray
        if dataArray[storedKey] = wsData
        inStore = true
        end if
        end for
        
        if inStore = false
        dataArray[key]=wsData
        end if
        m.dzBaseRegister.Write("eventData", FormatJson(dataArray))
        if flag
'            print "Processed data in storage : Count = "  dataArray.Count()
        end if
    end if
    
end if
end function

function retrieveEventData()
    sendCount = 0
    jsonArray = CreateObject("roArray", 8, true)
    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        if m.dzBaseRegister.Read("eventData") <> ""
          
            dataArray = parseJSON(m.dzBaseRegister.Read("eventData"))
            m.dzBaseRegister.Delete("eventData")
        end if
        
        if dataArray.Count() > 0 
            for each key in dataArray
                jsonArray.push(dataArray[key])  
            end for
                m.storedData = jsonArray
        end if
    end if
end function



function purgeEventData()
    ? "PURGE BATCHED EVENTS"
    jsonArray = CreateObject("roArray", 8, true)
    if m.dzBaseRegister.Exists("eventData")
        dataArray = {}
        if m.dzBaseRegister.Read("eventData") <> ""
            dataArray = parseJSON(m.dzBaseRegister.Read("eventData"))
            m.dzBaseRegister.Delete("eventData")
        end if  
        if dataArray.Count() > 0
            for each key in dataArray
                jsonArray.push((dataArray[key]))
            end for
            ? "PURGE "; (formatJSON(jsonArray))
'             wsSend(formatJSON(jsonArray))
'             jsonArray.Clear()
        end if
    end if
end function

' Function to call setSessionTime. This function will be called by both base and collector
function callSetSessionTime()
    setSessionTime()
end function


' Method to check if an event is been configured by customer.
Function checkIfEventConfigured(event as String) as boolean
    m.media_type = getMediaType()
    if m.top.events_v3 <> invalid
    for each eventV3 in m.top.events_v3
        If eventV3.name = event
            for each mType in eventV3.media_types
            if mType = m.media_type or mType = "na"
'               ? "TRIGGER EVENT V3:"; eventV3.name ;"="; event
'               ? "MEDIA TYPE FOR EVENT V3 = "; eventV3.media_types
                return true
                
            End If
            end for
        end if
    end for
  else
    If m.top.events <> invalid
        if m.top.events.types <> invalid
        For Each eventType in m.top.events.types
            If eventType.name = event
'               ? "TRIGGER EVENT V2:"; eventType.name ;"="; event
                return true
            End If
        end For
        end if
    End If
 end if
    return false
End Function

'Function to check if Fluxdata is available
Function checkIfFluxConfigured(event as String) as boolean   

    If m.top.events <> invalid 
        if m.top.events.flux_data <> invalid
        For Each fluxType in m.top.events.flux_data
            If fluxType = event
                return true
            End If
        end For
        end if
    End If
    return false
End Function

'Function to check if Metadata is available
Function checkIfMetaConfigured(event)  
    If m.top.events <> invalid   
        if m.top.events.metdata <> invalid
        For Each metaType in m.top.events.metdata
            If metaType = event
                return true
            End If
        end For
        end if
        
    End If
    return false
End Function

' Method to get unix epoch time stamp in millisecond 
Function getCurrentTimestampInMillis()
    dateObj = createObject("roDateTime")
    currentSeconds = dateObj.AsSeconds()
    m& = 1000
    currentMilliseconds = currentSeconds * m&
    ms = currentMilliseconds + dateObj.GetMilliseconds()
    return ms
End Function

' Set of functions to get values
' Start
' Returns device's unique ID
Function getUniqueDeviceId()
    return createObject("roDeviceInfo").GetChannelClientId()
End Function

Function getAdId()
    return createObject("roDeviceInfo").GetRIDA()
End Function

' Returns device's version
Function getVersion()
    return createObject("roDeviceInfo").GetVersion()
end function

' Returns Client IP
Function getIpAddress()
    return m.geoData.client_ip
end function

' Returns Client city
Function getCity()
    return m.geoData.city
end function

' Returns Client ZIP
Function getZip()
    return m.geoData.zip
end function

' Returns Client country code
Function getCountryCode()
    return m.geoData.countryCode
end function

' Returns Client country
Function getCountry()
    return m.geoData.country
end function

' Returns Client latitude
Function getLatitude()
    return m.geoData.lat
end function

' Returns Client longitude
Function getLongitude()
    return m.geoData.lon
end function

' Returns Client region code
Function getRegionCode()
    return m.geoData.regionCode
end function

' Returns Client region
Function getRegion()
    return m.geoData.region
end function

Function getasn()
    return m.geoData.asn
end function

Function getasnOrg()
    return m.geoData.asnOrganization
end function
Function getTimezone()
    return m.geoData.timezone
end function
Function getTimezoneOffset()
    return m.geoData.timezone_offset
end function
Function getContinent()
    return m.geoData.continent
end function
Function getContinentCode()
    return m.geoData.continentCode
end function
Function getDistrict()
    return m.geoData.district
end function

' Returns Client ISP
Function getISP()
    return m.geoData.isp
end function


' Returns device OS
Function getOS()
    return "Roku OS"
end function

' Returns device OS version
Function getOSVersion()
    ver = createObject("roDeviceInfo").GetOSVersion()
    version = ver.major + "."+ver.minor+"."+ver.revision+"."+ver.build
    return version
end function

' Returns device name
Function getModelDisplayName()
 return createObject("roDeviceInfo").GetModelDisplayName()
end function
' End

' Returns device type
Function getDeviceType()
return "ott device"
end Function
' End

' Returns video type
Function getVideoType()
return "Content"
end Function
' End

' Function to get random number
function randDigit(count)
    value = ""
    for i=1 to count
        value += Rnd(9).toStr()
    end for
    return value
end function

' Function to get session ID
Function getSessionData()
     sessionIdBool = 0
     if m.dzBaseRegister.Exists(m.sessionIdKey) and m.dzBaseRegister.Exists(m.sessionIdTimeKey)
         if(getCurrentTimestampInMillis() - Val(m.dzBaseRegister.Read(m.sessionIdTimeKey)) < 1260000)
            sessionIdBool = 1
            sessionId = m.dzBaseRegister.Read(m.sessionIdKey)
         end if
     end if
     if sessionIdBool = 0
        setSessionData()
     else
        return sessionId
     end if
End Function

'function to get sessionStartTimestamp
Function getSessionStartTimestamp()
return m.sessionStartTimestamp
'return Val (m.dzBaseRegister.Read(m.sessionIdTimeKey))
End Function

'Function to check session ID
Function checkSessionData()
     sessionIdBool = 0
     if m.dzBaseRegister.Exists(m.sessionIdKey) and m.dzBaseRegister.Exists(m.sessionIdTimeKey)
         if(getCurrentTimestampInMillis() - Val(m.dzBaseRegister.Read(m.sessionIdTimeKey)) < 1260000)
            sessionIdBool = 0
         end if
     end if
     if sessionIdBool = 0
        return false
     else
        return true
     end if
End Function

'Function to set session ID
function setSessionData()
    if m.videoCntKeyFlag
        sessionId = m.device.GetRandomUUID()
        m.dzBaseRegister.Write(m.sessionIdKey, sessionId)
        m.numberOfAdsPlayed = 0
        m.dzBaseRegister.Write(m.sessionIdTimeKey, getCurrentTimestampInMillis().ToStr())
        if m.dzBaseRegister.Exists(m.vewSessionIdKey)
            m.dzBaseRegister.Delete(m.vewSessionIdKey)
        end if
        if m.dzBaseRegister.Exists(m.noOfVideosKeyName)
            m.dzBaseRegister.Delete(m.noOfVideosKeyName)
        end if
        viewID = m.device.GetRandomUUID()
        m.dzBaseRegister.Write(m.vewSessionIdKey, viewID.ToStr())
        m.setViewIDFlag = false
        return m.dzBaseRegister.Read(m.sessionIdKey)
     else 
        return m.dzBaseRegister.Read(m.sessionIdKey)' ""
     end if
end function

'Function to set session Time
function setSessionTime()
        if m.dzBaseRegister.Exists(m.sessionIdKey)
            m.dzBaseRegister.Write(m.sessionIdTimeKey, getCurrentTimestampInMillis().ToStr())
            m.dzBaseRegister.Flush()
            return true
        else
            return false
        end if
end function

'Function to session view ID
function setSessionViewId()
        
        if m.dzBaseRegister.Exists(m.vewSessionIdKey)
           videoCnt = Val(m.dzBaseRegister.Read(m.vewSessionIdKey))
        end if
        videoCnt= m.device.GetRandomUUID()
        m.dzBaseRegister.Write(m.vewSessionIdKey, videoCnt.ToStr())
end function

'Function to get session View ID
function getSessionViewId()
    if m.dzBaseRegister.Exists(m.vewSessionIdKey)
        return m.dzBaseRegister.Read(m.vewSessionIdKey)     
    else
        return ""
    end if
end function

'Function to get player ready state
function getPlayerReadyState()
return m.playerReadyState.toStr()
end function

'Function to set current video URL
function setContentUrlToBase(videoURLCOL)
    m.videoURL = videoURLCOL
'    setNoOfVideos()
'    m.viewIDSet = true
end function

'Function to set number of videos in current session
function setNoOfVideos()
    ba = CreateObject("roByteArray") 
    ba.FromAsciiString(m.videoURL)
    getUrl = ba.ToBase64String()
    if getUrl <> ""
        vURL = getUrl
        if m.dzBaseRegister.Exists(m.noOfVideosKeyName)
            nVideoArray = {}
            nVideoArray = parseJSON(m.dzBaseRegister.Read(m.noOfVideosKeyName))
            if nVideoArray[vURL] <> invalid
                m.noOfVid = nVideoArray.Count()
            else
                nVideoArray[vURL] = getCurrentTimestampInMillis().ToStr()
                m.dzBaseRegister.Write(m.noOfVideosKeyName, FormatJson(nVideoArray))
                m.noOfVid = nVideoArray.Count()
                setSessionViewId()
            end if
          else
                nVideoArray = {}
                nVideoArray[vURL] = getCurrentTimestampInMillis().ToStr()
                m.dzBaseRegister.Write(m.noOfVideosKeyName, FormatJson(nVideoArray))
                m.noOfVid = 1
        end if
    end if
end function

'Function to get number of video during current session
function getNoOfVideos()
    return m.noOfVid.toStr()
end function
function getConnectionType()
    return createObject("roDeviceInfo").GetConnectionType()
end function
