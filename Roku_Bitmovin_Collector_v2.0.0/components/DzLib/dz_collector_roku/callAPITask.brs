Sub init()
    m.top.functionName = "getContent"
    getServerTime()
End Sub

Sub getContent()
    requestData = m.top.requestData
    resultObject = utils_HTTPRequest(requestData.httpMethodString, requestData.urlString, requestData.postBodyString, requestData.headersAssociativeArray)
    m.top.result = resultObject
End Sub

Sub getServerTime()
    requestData = {}
    requestData.httpMethodString = "GET"
    url = "https://broker.datazoom.io/broker/v1/getEpochMillis"
    requestData.urlString = url
    requestData.postBodyString = invalid
    requestData.headersAssociativeArray = invalid
    resultObject = utils_HTTPRequest(requestData.httpMethodString, requestData.urlString, requestData.postBodyString, requestData.headersAssociativeArray)
    m.top.serverTimeResult = resultObject
End Sub