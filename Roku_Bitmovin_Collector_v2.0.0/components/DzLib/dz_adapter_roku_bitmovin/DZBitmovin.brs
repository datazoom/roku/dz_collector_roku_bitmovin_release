Sub init()
    m.device = CreateObject("roDeviceInfo")
    m.global.observeField("input", "onInputReceived")
    
    'Init Timers
    m.playStateTimer = m.top.findNode("playStateTimer")
    m.dataTimer = m.top.findNode("dataTimer")
    m.quartileTimer = m.top.findNode("quartileTimer")
    
'    m.playStateTimer.ObserveField("fire","playStateTimerFunction")
    m.dataTimer.ObserveField("fire","dataTimerFunction")
    m.quartileTimer.ObserveField("fire","quartileTimerFunction")
    
    'Init Templates
    m.template = {}
    m.template.event = {}
    m.template.user_details = {}
    m.template.device = {}
    m.template.player ={}
    m.template.video ={}
    m.template.geo_location = {}
    m.template.network = {}
    m.customSessionMeta = {}
    m.customPlayerMeta = {}
    m.template.event.attributes = {}
    m.template.ad = {}
    m.template.page = {}
    m.template.ops_metadata = {}
    m.template.custom = CreateObject("roAssociativeArray")
    m.oldposition=0
    m.playerPlay = false
    m.playerPlayRequest = false
    m.playerResume = false
    m.bufferStarted = false
    m.stallStarted = false
    m.videoPaused = false
    m.metatitle = ""
    m.streamingType = ""

    m.eventConfig = invalid
    m.bufferFillVal = 0
    m.contentFirstQuartile = true
    m.contentSecondQuartile = true
    m.contentThirdQuartile = true
    m.quartilePresence = false
    m.renditionName = ""
    m.renditionBitrate = 0
    m.renditionVideoBitrate = 0
    m.renditionAudioBitrate = 0
    m.streamBitrate = 0
    m.mute = false
    m.defaultMute = false
    m.playCounter = 0
    m.defaultRate = 1
    m.milestonePercent = 0
    m.playerHeight = 0
    m.playerWidth = 0
    m.renditionHeight = 0
    m.renditionWidth = 0
    m.streamBitrate = 0
    m.systemBitrate = 0
    m.pausePoint = 0
'    m.sessionStartTimestamp = 0
    m.numberOfErrors = 0
    m.bufferDuration = 0
    m.bufferStartTime = 0
    m.adBufferStartTime = 0
    m.stallStartTime = 0
    m.bufferDurationAds = 0
    m.bufferDurationContent = 0
    m.playHead = 0
    m.seekStartPoint = 0
    m.cnf = {}
    m.contentSessionId = 0
    m.assetID = 0
    m.mute = false
    m.adQuartile = 0
    m.adsQV = {}
    m.contentsQV = {}
    
    m.TSBUFFERSTART = 0
    m.TSSTALLSTART = 0
    m.TSADSTALLSTART = 0
    m.TSLASTHEARTBEAT = 0
    m.TSLASTMILESTONE = 0
    m.TSLASTADHEARTBEAT = 0
    m.TSLASTADMILESTONE = 0
    m.TSPAUSED = 0
    m.TSREQUESTED = 0
    m.TSSTARTED = 0
    m.TSADSTARTED = 0
    m.TSLOADED = 0
    m.TSRENDITIONCHANGE = 0
    m.playerAction = ""
    m.TSADIMPRESSION = 0
    m.TSADBREAKSTART = 0
    m.adSeekStartTime = 0
    m.seekStartTime = 0
    m.networkBandwidth = 0
    
    m.fired10 = false
    m.fired25 = false
    m.fired50 = false
    m.fired75 = false
    m.fired90 = false
    m.fired95 = false
    
    m.tempCustomMeta = {}
    
    m.adFired10 = false
    m.adFired25 = false
    m.adFired50 = false
    m.adFired75 = false
    m.adFired90 = false
    m.adFired95 = false
    m.lastAdMilestoneTime = 0
    m.lastAdHeartbeatTime = 0
    m.lastAdTime = 0
    
    m.videoType = "content"
    m.engagementContentStart = 0
    m.engagementContendDuration = 0 
    m.oldAdTime = 0
    m.adTime = 0
    m.playContentDuration = 0
    m.playbackDuration = 0
    m.adMilestoneTime = 0
    m.adUrl = ""
    m.adTitle = ""
    m.adServer = ""
    m.adSystem = ""
    m.adDuration = 0
    
    m.playbackStallCount = 0
    m.playbackStallCountAds = 0
    m.playbackStallCountContent = 0
    
    m.playbackStallDuration = 0
    m.playbackStallDurationAds = 0
    m.playbackStallDurationContent = 0
    m.tmpPlaybackTimer = 0
    m.pauseDuration = 0 
    m.pauseDurationAds = 0
    m.pausedDurationTotal = 0
    m.firstFrameTriggered = false
    m.playbackDurationContent = 0
    m.heartCount = 1
    m.playbackCompleteTriggered = false
    m.playRequestTriggered = false
    m.playingDuration = 0
    m.eventCount = 0
    m.contentSessionStart = 0
    m.contentRequestCount = 0
    m.numberOfErrorsContent = 0
    m.numberOfContentPlays = 0
    m.prevState = Invalid
    m.currentState = Invalid
    m.oldTimeAd = 0
End Sub

'Function to get player Type
function playerType()
    return "bitmovin"
end function

'Function to get deviceType
function deviceType()
    return "ott device"
end function
function getMediaType()
return LCase(m.videoType)
end function

'Function containing Datapoint List
Function getLibConfig()
    if m.config = invalid
        m.config = {
          events: {
            EVENTPLAY: "play",
            EVENTPAUSE: "pause",
            EVENTBUFFERING: "buffering",
            EVENTBUFFEREND: "buffer_end",
            EVENTBUFFERSTART: "buffer_start",
            EVENTSTALLSTART: "stall_start",
            EVENTSTALLEND: "stall_end",
            EVENTPLAYBACKSTART: "playback_start",
            EVENTPLAYBACKCOMPLETE: "playback_complete",
            EVENTPLAYREQUEST: "media_request",
            EVENTPLAYING: "playing",
            EVENTRESUME: "resume",
            EVENTPLAYERREADY: "player_ready",
            EVENTCFQ: "Content_First_Quartile",
            EVENTCSQ: "Content_Second_Quartile",
            EVENTCTQ: "Content_Third_Quartile",
            EVENTMUTE: "Mute",
            EVENTUNMUTE: "Unmute",
            EVENTBITRATECHANGE: "Bitrate_Change",
            EVENTADREQUEST: "Ad_Request",
            EVENTADCOMPLETE: "Ad_Complete",
            EVENTADIMPRESSION: "ad_impression",
            EVENTDATAZOOMLOADED: "datazoom_loaded",
            EVENTCUSTOM: "Custom_Event",
            EVENTHEARTBEAT: "heartbeat",
            EVENTCONTENTLOADED: "media_loaded",
            EVENTMILESTONE: "milestone",
            EVENTRENDITIONCHANGE: "rendition_change",
            EVENTSEEKSTART: "seek_start",
            EVENTSEEKEND: "seek_end",
            EVENTERROR: "error",
            EVENTADCLICK:"Ad_Click",
            EVENTADLOADED: "media_loaded",
            EVENTADPLAY: "playback_start",
            EVENTADERROR: "error",
            EVENTADRENDITIONCHANGE: "Ad_Rendition_Change",
            EVENTADPAUSE: "pause",
            EVENTADRESUME: "resume",
            EVENTADBREAKSTART: "ad_break_start",
            EVENTADBREAKEND: "ad_break_end",
            EVENTADCOMPLETE: "playback_complete",
            EVENTADREQUEST: "media_request",
            EVENTPLAYBTN: "play_btn",
            EVENTQVIEW: "qualified_view",
       
            METAQVTRIGGER: "qualified_view_sec",   
            METADURATION: "duration_sec",
            METAIP: "client_ip",
            METACITY: "city",
            METACOUNTRYCODE: "country_code",
            METAREGIONCODE: "region_code",
            METAOS: "os_name",
            METADEVICETYPE: "device_type",
            METADEVICEID: "device_id",
            METADEVICENAME: "device_name",
            METADEVICEMFG: "device_mfg",
            METAOSVERSION: "os_version",
            METAASN: "asn",
            METAASNORG: "asn_org",
            METAISP: "isp",
            METACOUNTRY: "country",
            METAREGION: "region",
            METAZIP: "postal_code",
            METATIMEZONE: "timezone_name",
            METATIMEZONEOFFSET: "timezone_offset",
            METACONTINENT: "continent",
            METACONTINENTCODE: "continent_code",
            METADISTRICT: "district",
            
            METACONTROLS: "controls",
            METALOOP: "loop",
            METAREADYSTATE: "readyState",
            METASESSIONVIEWID: "app_session_id",
            METAVIEWID: "content_session_id",
            METALONGITUDE: "longitude",
            METALATITUDE: "latitude",
            METADESCRIPTION: "description",
            METATITLE: "title",
            METAVIDEOTYPE: "media_type",
            METADEFAULTMUTED: "defaultMuted",
            METAISMUTED: "isMuted",
            METASOURCE: "source",
            METACUSTOM: "customMetadata",
            METADEFAULTPLAYBACKRATE: "defaultPlaybackRate",
            METAMILESTONEPERCENT: "milestone_percent",
            METAABSSHIFT: "absShift",
            METASEEKENDPOINT: "seek_end_point",
            METASEEKSTARTPOINT: "seek_start_point",
            
            METASESSIONSTARTTIMESTAMP: "app_session_start_ts_ms",
            
            METAPLAYERHEIGHT: "playerHeight",
            METAPLAYERWIDTH: "playerWidth",
            METAFRAMERATE: "frameRate",
            METAADVERTISINGID: "advertising_id",
            METAASSETID: "asset_id",
            METAERRORCODE: "error_code",
            METAERRORMSG: "error_msg",
            METAADPOSITION: "ad_position",
            METAUSERAGENT: "user_agent",
            METAPLAYERVERSION: "player_version",
            METADZSDKVERSION: "dz_sdk_version",
            METACONNECTIONTYPE: "connection_type",
            METAEVENTCOUNT: "event_count",
            METASTREAMINGPROTOCOL: "streaming_protocol",
            METASTREAMINGTYPE: "streaming_type",
            METATOTALSTARTUPDURATION: "startup_duration_total_ms",
            METASTARTUPDURATIONCONTENT: "startup_duration_content_ms",
            
            METAADSYSTEM: "ad_system",
            METAADBREAKID: "ad_break_id",
            METAADID: "ad_id",
            METAADCREATIVEID: "ad_creative_id",
            METAADSESSIONID: "ad_session_id",
            
            FLUXPLAYHEADUPDATE: "playhead_position_sec",
            FLUXBUFFERFILL: "buffer_fill_percent",
            FLUXPLAYERSTATE: "player_state",
            FLUXNUMBEROFVIDEOS: "numberOfVideos",
            FLUXRENDITIONBITRATE: "renditionBitrate",
            FLUXTSCONTENTBUFFERSTART: "time_since_last_buffer_start_content_ms",
            FLUXTSBUFFERSTART: "time_since_last_buffer_start_ms",
            FLUXTSCONTENTSTALLSTART: "time_since_last_stall_start_content_ms",
            FLUXTSADSTALLSTART: "time_since_last_stall_start_ad_ms",
            FLUXTSSTALLSTART: "time_since_last_stall_start_ms"
            FLUXTSLASTHEARTBEAT: "time_since_last_heartbeat_ms",
            FLUXTSLASTMILESTONE: "time_since_last_milestone_content_ms",
            FLUXTSPAUSED: "time_since_last_pause_ms",
            FLUXTSREQUESTED: "time_since_request_content_ms",
            FLUXTSSTARTED: "time_since_started_content_ms",
            FLUXPLAYBACKDURATION: "playback_duration_ms",
            FLUXNUMBEROFADS: "numberOfAds",
            FLUXBITRATE: "bitrate",
            FLUXVIEWSTARTTIME: "viewStartTimestamp",
            FLUXRENDITIONHEIGHT: "renditionHeight",
            FLUXRENDITIONWIDTH: "renditionWidth",
            FLUXRENDITIONNAME: "renditionName",
            FLUXRENDITIONVIDEOBITRATE: "rendition_video_bitrate_kbps",
            FLUXRENDITIONAUDIOBITRATE: "renditionAudioBitrate",
            FLUXTSLASTRENDITIONCHANGE: "time_since_last_rendition_change_ms",
            FLUXNUMBEROFERRORS: "num_errors",
            FLUXNUMBEROFERRORSCONTENT: "num_errors_content",
            FLUXTSLASTADMILESTONE: "time_since_last_milestone_ad_ms",
            FLUXTSLASTADHEARTBEAT: "time_since_last_heartbeat_ad_ms",
            FLUXBUFFERDURATION: "buffer_duration_ms",
            FLUXBUFFERDURATIONCONTENT: "buffer_duration_content_ms",
            FLUXBUFFERDURATIONADS: "buffer_duration_ads_ms",
            FLUXENGAGEMENTDURATION: "engagement_duration_ms",
            FLUXENGAGEMENTDURATIONCONTENT: "engagement_duration_content_ms",
            FLUXPLAYBACKDURATIONCONTENT: "playback_duration_content_ms",
            FLUXPLAYBACKSTALLDURATIONCONTENT: "stall_duration_content_ms",
            FLUXPLAYBACKSTALLDURATIONADS: "stall_duration_ads_ms",
            FLUXPLAYBACKSTALLDURATION: "stall_duration_ms",
            FLUXPLAYBACKSTALLCOUNTCONTENT: "stall_count_content",
            FLUXPLAYBACKSTALLCOUNTADS: "stall_count_ads",
            FLUXPLAYBACKSTALLCOUNT: "stall_count",
            
            FLUXNUMBEROFADSPLAYED: "num_ad_plays",
            FLUXNUMBEROFCONTENTPLAYS: "num_content_plays"
            FLUXNUMBEROFCONTENTREQUEST: "num_requests_content"
            FLUXENGAGEMENTDURATIONADS: "engagementDurationAds",
            FLUXPLAYBACKDURATIONADS: "playback_duration_ads_ms",
            FLUXTSLASTAD: "time_since_last_ad_completed_ms"
            FLUXTSADBREAKSTART: "time_since_last_ad_break_start_ms",
            FLUXTSADREQUESTED: "time_since_last_request_ad_ms",
            FLUXTSADSTARTED: "time_since_last_started_ad_ms",
            FLUXTSADBUFFERSTART: "time_since_last_buffer_start_ad_ms",
            FLUXBANDWIDTH: "bandwidth_kbps"
            FLUXTSADSEEKSTART: "timeSinceAdSeekStart",
            FLUXTSSEEKSTART: "time_since_last_seek_start_ms",
            FLUXCONTENTSESSIONSTART:"content_session_start_ts_ms"
            FLUXPAUSEDURATION: "pause_duration_ms",
            FLUXPAUSEDURATIONCONTENT: "pause_duration_content_ms",
            FLUXPAUSEDURATIONADS: "pause_duration_ads_ms"
          }
        }
    end if
    return m.config
end Function
'--------------------------- Custom event and meta functions ----------------------------
function generateDatazoomEvent(customEvent as String, customEventMeta = invalid)
'    configureEvents()
    playerMetrics()
    if customEventMeta <> invalid
    m.tempCustomMeta.Append(m.template.custom)
    m.template.custom.append (customEventMeta)
    wsSend(getMessageTemplate("custom_"+customEvent))
    m.template.custom = {}
    m.template.custom.Append(m.tempCustomMeta)
    end if
    wsSend(getMessageTemplate("custom_"+customEvent))
End function  

Function setDZSessionMeta(key = invalid, value = invalid, customMetadata = invalid)
    if type(key) = "roAssociativeArray"
    m.customSessionMeta.append (key)
    else if key <> invalid and value <> invalid
    m.customSessionMeta[key] = value
    end if
End Function

Function setDZPlayerMeta(key = invalid, value = invalid, customMetadata = invalid)
    if type(key) = "roAssociativeArray"
    m.customPlayerMeta.append (key)
    else if key <> invalid and value <> invalid
    m.customPlayerMeta[key] = value
    end if
End Function

Function getDZSessionMeta()
   customMetaString = formatJson(m.customSessionMeta)
   return customMetaString
End Function

Function getDZPlayerMeta()
   customMetaString = formatJson(m.customPlayerMeta)
   return customMetaString
End Function

Function rmDZSessionMeta()
  m.customSessionMeta = {}
End Function

Function rmDZPlayerMeta()
    m.customPlayerMeta = {}
End Function

'----------------------------------------------------------------------------------------

'------------- Ad Events and Meta ------------------------------------------------------

function generateAdEvent(adData = invalid)

if adData <> invalid
? "MOMO AD DATA = "; adData
    playerMetrics()
    If checkIfMetaConfigured(m.eventConfig.METAADSYSTEM) and adData.adserver <> invalid
         m.template.ad["adSystem"] = adData.adserver
    end if
    If checkIfMetaConfigured("ad_duration_sec")
         m.template.ad["ad_duration_sec"] = adData.duration
    end if
    
if adData.rendersequence <> invalid then m.template.ad["ad_position"] = adData.rendersequence
If adData.type = "AdRequest"
        If checkIfMetaConfigured(m.eventConfig.METAADSESSIONID)
         m.template.ad["ad_session_id"] = m.device.GetRandomUUID()
    end if
          m.adBufferStartTime = getCurrentTimestampInMillis()
          m.TSADSTARTED = getCurrentTimestampInMillis()
'          m.videoType = "ad"
          
          if checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
          wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST)) 
          end  if
end if
If adData.type = "PodStart"
          m.TSADBREAKSTART = getCurrentTimestampInMillis()
          if checkIfEventConfigured(m.eventConfig.EVENTADBREAKSTART)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADBREAKSTART))
          end If  
end if

If adData.type = "Impression"

           m.TSADIMPRESSION = getCurrentTimestampInMillis()
          if checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
          wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED)) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTADPLAY)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADPLAY)) 
          end If
            If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If
          if checkIfEventConfigured(m.eventConfig.EVENTADIMPRESSION)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADIMPRESSION)) 
          end If
          m.videoType = "ad"
          m.adUrl = adData.ad.streams[0].url
          m.adTitle = adData.Ad.adtitle
          m.template.ad["ad_break_id"] = adData.ad.adid
end if 

If adData.type = "Complete"
          if checkIfEventConfigured(m.eventConfig.EVENTADCOMPLETE)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADCOMPLETE)) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTADBREAKEND)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADBREAKEND)) 
          end If
          m.numberOfAdsPlayed = m.numberOfAdsPlayed + 1
          m.videoType = "content"
          m.engagementContentStart = getCurrentTimestampInMillis()
          m.lastAdTime = getCurrentTimestampInMillis()
          m.adFired10 = false
          m.adFired25 = false
          m.adFired50 = false
          m.adFired75 = false
          m.adFired90 = false
          m.adFired95 = false  
end if 

If adData.type = "Pause"
          if checkIfEventConfigured(m.eventConfig.EVENTADPAUSE)
          wsSend(getMessageTemplate("pause")) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
          wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN)) 
          end If
end if

If adData.type = "Resume"
        if checkIfEventConfigured(m.eventConfig.EVENTADPAUSE)
          wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLEND))
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
          wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN)) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTADRESUME)
          wsSend(getMessageTemplate("resume")) 
          end If
          If checkIfEventConfigured(m.eventConfig.EVENTPLAY)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
          End If
          If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If
end if

If adData.errMsg <> invalid
    print "*****   Error message: " + adData.errMsg + adData.errCode
    m.template.event.attributes[m.eventConfig.METAERRORCODE] = adData.errCode
    m.template.event.attributes[m.eventConfig.METAERRORMSG] = adData.errMsg
    wsSend(getMessageTemplate("error"))
    m.template.event.attributes = {}
end if

end if  

'if adData.duration <> invalid and adData.time <> invalid and adData.time < adData.duration
'    if adData.time / adData.duration > 0.1 and m.adFired10 = false 
'    m.template.event.attributes["milestonePercent"] = 0.10
'    wsSend(getMessageTemplate("milestone"))
'    m.adFired10 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'    if adData.time / adData.duration > 0.25 and m.adFired25 = false 
'    m.template.event.attributes["milestonePercent"] = 0.25
'    wsSend(getMessageTemplate("milestone")) 
'    m.adFired25 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'    if adData.time / adData.duration > 0.50 and m.adFired50 = false 
'    m.template.event.attributes["milestonePercent"] = 0.50
'    wsSend(getMessageTemplate("milestone"))
'    m.adFired50 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'     
'    end if
'    if adData.time / adData.duration > 0.75 and m.adFired75 = false
'    m.template.event.attributes["milestonePercent"] = 0.75 
'    wsSend(getMessageTemplate("milestone"))  
'    m.adFired75 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'    if adData.time / adData.duration > 0.85 and m.adFired90 = false 
'    m.template.event.attributes["milestonePercent"] = 0.90
'    wsSend(getMessageTemplate("milestone"))
'    m.adFired90 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'    if adData.time / adData.duration > 0.90 and m.adFired95 = false 
'    m.template.event.attributes["milestonePercent"] = 0.95
'    wsSend(getMessageTemplate("milestone"))
'    m.adFired95 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'end if
'
'if m.top.events.interval <> invalid and adData <> invalid
'
'if adData.time <> invalid and adData.time = 1
'm.adTime = 1
'm.playbackDuration = m.playbackDuration + 1
'
'else if m.adTime > 0
'm.adTime = m.adTime + 1
'm.playbackDuration = (m.playbackDuration + 1)
'
'end if
'
'If (m.top.events.interval/1000) - (m.adTime - m.oldAdTime) = 0 
'wsSend(getMessageTemplate("Heartbeat"))
'm.oldAdTime = m.adTime
'm.lastAdHeartbeatTime = getCurrentTimestampInMillis()
'end if
'
'end if

End function  

' Configure maximum possible events needed to be collected from player.
Sub configureEvents() 
    m.player = m.top.playerInit.player
    m.bitmovinFunctions = m.player.BitmovinFunctions
    m.BitmovinFields = m.player.BitmovinFields
    m.BitmovinPlayerState = m.player.BitmovinPlayerState
    m.playerConfig = m.player.callFunc(m.bitmovinFunctions.GET_CONFIG, invalid)
    'Player Observers
    m.player.ObserveField(m.BitmovinFields.PLAYER_STATE, "OnVideoPlayerStateChange")
    m.player.ObserveField(m.BitmovinFields.ERROR, "onVideoError")
    m.player.ObserveField(m.BitmovinFields.SEEK, "onSeek")
    m.player.ObserveField(m.BitmovinFields.SEEKED, "onSeeked")
    m.player.ObserveField(m.BitmovinFields.IMPRESSION, "onImpression")
    m.player.ObserveField(m.BitmovinFields.CURRENT_TIME, "OnHeadPositionChange")
    m.player.ObserveField(m.BitmovinFields.SOURCE_LOADED, "OnContentLoaded")
    m.player.ObserveField(m.bitmovinFields.VIDEO_DOWNLOAD_QUALITY_CHANGED, "onVideoQualityChanged")
    m.player.ObserveField(m.BitmovinFields.DESTROY, "OnPlayerExit")
'    m.player.ObserveField(m.bitmovinFields.UNLOAD, "onVideoFinished")
    
    'Ad Observers
    m.player.ObserveField(m.bitmovinFields.AD_BREAK_STARTED, "onAdBreakStart")
    m.player.ObserveField(m.bitmovinFields.AD_BREAK_FINISHED, "onAdBreakFinish")
    m.player.ObserveField(m.bitmovinFields.AD_STARTED, "onAdStart")
    m.player.ObserveField(m.bitmovinFields.AD_FINiSHED, "onAdFinish")
    m.player.ObserveField(m.bitmovinFields.AD_ERROR, "onAdError")
    m.player.ObserveField(m.bitmovinFields.AD_SKIPPED, "onAdSkipped")
    m.player.ObserveField(m.bitmovinFields.AD_QUARTILE, "onAdQuartile")
    m.player.ObserveField(m.bitmovinFields.AD_INTERACTION, "onAdInteraction")
    
    m.defaultRate = 1
    m.numberOfErrors = 0
    m.displayMode = m.device.getDisplayMode()
    m.displaySize = m.device.getDisplaySize()
    m.playerHeight = m.displaySize.h
    m.playerWidth = m.displaySize.w 
End Sub

' Checks if specific data points is available or not
function dataPointValidation()
    If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
        m.quartilePresence = true
    End If
end function

Sub onImpression()
? "DZ-PRINT ON IMPRESSION"
end Sub

Sub onVideoFinished()
        m.quartileTimer.control = "stop"
        m.dataTimer.control = "stop"
        m.playStateTimer.control = "stop"
        m.player.callFunc(m.bitmovinFunctions.UNLOAD, invalid)
        m.firstFrameTriggered = false
        m.playbackCompleteTriggered = true
        m.customPlayerMeta = {}
        m.playbackTimer = Invalid              
        m.playRequestTriggered = false
        m.template.custom = {}
        m.playingDuration = 0
        m.playbackDurationContent = 0
        m.TSLASTADHEARTBEAT = 0
end sub

Sub onPlayerExit()
        setState()
        m.quartileTimer.control = "stop"
        m.dataTimer.control = "stop"
        m.playStateTimer.control = "stop"
        m.player.callFunc(m.bitmovinFunctions.DESTROY, invalid)
        m.firstFrameTriggered = false
        m.playbackCompleteTriggered = true
        m.customPlayerMeta = {}
        m.playbackTimer = Invalid              
        m.playRequestTriggered = false
        m.template.custom = {}
        m.playingDuration = 0
        m.playbackDurationContent = 0
        m.TSLASTADHEARTBEAT = 0
end sub

' On Ad Functions
Sub onAdBreakStart()
'    m.videoType = "ad"
    m.TSADBREAKSTART = getCurrentTimestampInMillis()
          
          If checkIfMetaConfigured(m.eventConfig.METAADBREAKID)
          m.template.ad["ad_break_id"] = m.device.GetRandomUUID()
          end if
          If checkIfMetaConfigured(m.eventConfig.METAADSESSIONID)
          m.template.ad["ad_session_id"] = m.device.GetRandomUUID()
          end if
          If checkIfMetaConfigured(m.eventConfig.METAADPOSITION)
          if m.player.adBreakStarted.adBreak.scheduleTime = 0 
          m.template.ad["ad_position"] = "pre"
          end if
          if m.player.adBreakStarted.adBreak.scheduleTime > 0 
          m.template.ad["ad_position"] = "mid"
          end if
          end if
          if checkIfEventConfigured(m.eventConfig.EVENTADBREAKSTART)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADBREAKSTART))
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
          wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST)) 
          end  if
end sub

Sub onAdBreakFinish()
'? "DZ-PRINT: AD BREAK FINISH"; m.player.adBreakFinished
    if checkIfEventConfigured(m.eventConfig.EVENTADBREAKEND)
       wsSend(getMessageTemplate(m.eventConfig.EVENTADBREAKEND)) 
    end If
m.videoType = "content"


End sub

Sub onAdStart()
m.videoType = "ad"
? "MOMO adQV = "; m.top.adQV
m.adsQV = m.top.adQV
m.adsMilestones = m.top.adMilestones
m.TSADIMPRESSION = getCurrentTimestampInMillis()
m.TSADSTARTED = getCurrentTimestampInMillis()
m.adTitle = m.player.adStarted.ad.data.adtitle

m.adCreativeId = m.player.adStarted.ad.data.creativeid
If checkIfMetaConfigured(m.eventConfig.METAADCREATIVEID)
    m.template.ad["ad_creative_id"] = m.adCreativeid
end if

m.adsId = m.player.adStarted.ad.data.adid
If checkIfMetaConfigured(m.eventConfig.METAADID)
    m.template.ad["ad_id"] = m.adsId
end if
m.adDuration = m.player.adStarted.ad.data.duration
If checkIfMetaConfigured("ad_duration_sec")
         m.template.ad["ad_duration_sec"] = m.adDuration
end if
m.adServer = m.player.adStarted.ad.data.adserver
m.adSystem = m.player.adStarted.ad.data.adsystem
If checkIfMetaConfigured(m.eventConfig.METAADSYSTEM)
         m.template.ad["ad_system"] = m.adSystem
end if

if checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
          wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED)) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTADPLAY)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADPLAY)) 
          end If
            If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If
          if checkIfEventConfigured(m.eventConfig.EVENTADIMPRESSION)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADIMPRESSION)) 
          end If
End Sub

Sub onAdFinish()
        if checkIfEventConfigured(m.eventConfig.EVENTADCOMPLETE)
          wsSend(getMessageTemplate(m.eventConfig.EVENTADCOMPLETE)) 
          end If
          m.adMilestoneTime = 1
          m.numberOfAdsPlayed = m.numberOfAdsPlayed + 1
          m.videoType = "content"
          m.engagementContentStart = getCurrentTimestampInMillis()
          m.lastAdTime = getCurrentTimestampInMillis()
          m.adFired10 = false
          m.adFired25 = false
          m.adFired50 = false
          m.adFired75 = false
          m.adFired90 = false
          m.adFired95 = false  
End Sub

Sub onAdError()
m.videoType = "ad"
    m.numberOfErrors = m.numberOfErrors + 1
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFERRORS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFERRORS] = m.numberOfErrors
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFERRORSCONTENT)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFERRORSCONTENT] = m.numberOfErrorsContent
    end if
    m.template.event.attributes[m.eventConfig.METAERRORCODE] = m.player.adError.data.errCode
    m.template.event.attributes[m.eventConfig.METAERRORMSG] = m.player.adError.data.errMsg
    wsSend(getMessageTemplate("error"))
    m.template.event.attributes = {}
End Sub

Sub OnAdQuartile()
m.adQuartile = m.adQuartile + 0.25
End Sub

Sub OnAdInteraction()
? "DZ-PRINT: AD INTERACTION"; m.player.adInteraction
End Sub

Sub OnAdSkipped()
? "DZ-PRINT: AD SKIPPED"; m.player.adSkipped
End Sub

Sub onVideoError()
         m.numberOfErrors = m.numberOfErrors + 1
       if m.videoType = "content" and m.streamingType <> "Live"
        m.numberOfErrorsContent = m.numberOfErrorsContent + 1
        end if

        If checkIfEventConfigured(m.eventConfig.EVENTERROR)
          m.template.event.attributes[m.eventConfig.METAERRORCODE] = m.player.focusedChild.errorCode
          m.template.event.attributes[m.eventConfig.METAERRORMSG] = m.player.focusedChild.errorMsg
          wsSend(getMessageTemplate(m.eventConfig.EVENTERROR))    
          m.template.event.attributes = {}  
        end if  
end Sub

' Player callback method on each seconds. Use to get the user current position in the playback. 
Sub OnHeadPositionChange()
    if m.quartilePresence
'        resetQuartileTimerFunction()
    end if    
    If m.top.events <> invalid
        if m.top.events.flux_data <> invalid
            fluxAvail = false
           For Each fluxType in m.top.events.flux_data
            fluxAvail=true
           end for
'           print fluxCount
           if fluxAvail
                    m.oldposition=m.player.position
'                        fluxMetricsData()
'                        ? "DZ-PRINT: HEARTBEAT = "; m.playbackTimer.TotalMilliseconds(); m.top.events.interval; m.heartCount
                        if m.playbackTimer.TotalMilliseconds() >= m.top.events.interval * m.heartCount
                        wsSend(getMessageTemplate("heartbeat"))
                        m.TSLASTHEARTBEAT = getCurrentTimestampInMillis()
                        m.heartCount = m.heartCount + 1  
                        end if
             if m.contentsQV.Count() > 0
'                        ? "MOMO CONTENT QV ALL = "; m.contentsQV
'                        ? "MOMO CONTENT QV CURRENT = "; m.contentsQV[0]
'                        ?"MOMO CONTENT TIME = "; m.playbackTimer.TotalMilliseconds()/1000
                   if m.playbackTimer.TotalMilliseconds()/1000 >= m.contentsQV[0]

                        m.template.event.attributes["qualified_view_sec"] = m.contentsQV[0]
                        wsSend(getMessageTemplate(m.eventConfig.EVENTQVIEW))
                        m.template.event.attributes = {}
                        if m.contentsQV.count() > 1
                             m.contentsQV.Shift()
                        else
                             m.contentsQV.Clear()
                        end if
'                    ? "MOMO CONTENT QV NEXT = "; m.contentsQV[0]
       
                    end if
                        
              end if
            end if
        end if
    end if
    'Calls base to set session time
    callSetSessionTime()
End  Sub

sub onVideoQualityChanged()

if m.prevState <> "ready"
m.TSRENDITIONCHANGE = getCurrentTimestampInMillis()
    If checkIfEventConfigured(m.eventConfig.EVENTRENDITIONCHANGE)
        wsSend(getMessageTemplate(m.eventConfig.EVENTRENDITIONCHANGE))
        m.template.event.attributes = {}
    end if
end if
end sub

function fluxMetricsData()
    m.template.event.metrics = {}
    m.template.event.metrics[m.eventConfig.METAEVENTCOUNT] = m.eventCount
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYHEADUPDATE)
        playHead = 0
        if m.player.currentTime <> invalid       
                playHead = m.player.currentTime
            m.template.event.metrics[m.eventConfig.FLUXPLAYHEADUPDATE] = playHead
        end if
        
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATION)
                if m.playbackTimer <> invalid
                m.playingDuration = (m.playbackTimer.TotalMilliseconds() - m.bufferDuration - m.pauseDuration) + (m.playbackDuration * 1000)
                else
                m.playingDuration = (m.playbackDuration * 1000) '+ (playHead * 1000)
               
                end if

        if m.playingDuration > 0
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = m.playingDuration
        else
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = 0
        end if
        m.playingDuration = 0
    end if
    
'    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONCONTENT)
'        m.playbackDurationContent = 0
'        if m.player <> invalid
'            if m.player.position <> invalid
'                playbackDurationContent = m.player.position
'            end if
'        end if
        if m.playbackTimer <> invalid
                m.playbackDurationContent = m.playbackTimer.TotalMilliseconds() - m.bufferDuration - m.pauseDuration
                else
              m.playbackDurationContent = m.player.currentTime * 1000  
        end if  
        If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONCONTENT)
'        m.playbackDurationContent = 0
        if m.playbackDurationContent > 0
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = m.playbackDurationContent
        else
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 0
        end if
    end if
    
      if checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONADS)
    if m.bufferDuration > 0
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONADS] = m.playbackDuration * 1000
    end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXPAUSEDURATION)
        if m.player <> invalid
        m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATION] = m.pauseDuration + m.pauseDurationAds
        else
        m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATION] = 0
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXPAUSEDURATIONCONTENT)
        if m.player <> invalid
        m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATIONCONTENT] = m.pauseDuration
        else
        m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATIONCONTENT] = 0
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXPAUSEDURATIONADS)
        if m.player <> invalid
        m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATIONADS] = m.pauseDurationAds
        else
        m.template.event.metrics[m.eventConfig.FLUXPAUSEDURATIONADS] = 0
        end if
    end if
    
    
    
    
    if checkIfFluxConfigured(m.eventConfig.FLUXBUFFERDURATION)
    if m.bufferDuration > 0
        m.template.event.metrics[m.eventConfig.FLUXBUFFERDURATION] = m.bufferDuration
    end if
    end if
    
    if checkIfFluxConfigured(m.eventConfig.FLUXBUFFERDURATIONCONTENT)
    if m.bufferDurationContent > 0
        m.template.event.metrics[m.eventConfig.FLUXBUFFERDURATIONCONTENT] = m.bufferDurationContent
    end if
    end if
    
    if checkIfFluxConfigured(m.eventConfig.FLUXBUFFERDURATIONADS)
    if m.bufferDurationAds > 0
        m.template.event.metrics[m.eventConfig.FLUXBUFFERDURATIONADS] = m.bufferDurationAds
    end if
    end if
    
    
    If checkIfFluxConfigured(m.eventConfig.FLUXBUFFERFILL)
        m.template.event.metrics[m.eventConfig.FLUXBUFFERFILL] = m.bufferFillVal
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYERSTATE)
        playerState = ""
        if m.player <> invalid
            if m.player.playerState <> invalid
                playerState = m.player.playerState
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXPLAYERSTATE] = playerState
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFVIDEOS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFVIDEOS] = Val(getNoOfVideos())
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFERRORS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFERRORS] = m.numberOfErrors
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFERRORSCONTENT)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFERRORSCONTENT] = m.numberOfErrorsContent
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFADS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFADS] = 1
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFADSPLAYED) and m.numberOfAdsPlayed <> invalid
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFADSPLAYED] = m.numberOfAdsPlayed
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONBITRATE)
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONBITRATE] = m.renditionBitrate
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONVIDEOBITRATE)
        if m.player.focusedChild <> invalid
            if m.player.focusedChild.downloadedSegment <> invalid
'                if m.player.streamingSegment.segType = 2
                m.renditionVideoBitrate = m.player.focusedChild.downloadedSegment.bitrateBps
'                end if
            else
            m.renditionVideoBitrate = m.renditionBitrate
           end if
            
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONVIDEOBITRATE] = m.renditionVideoBitrate
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONAUDIOBITRATE)
     if m.player <> invalid
            if m.player.streamingSegment <> invalid
                if m.player.streamingSegment.segType = 1
                m.renditionAudioBitrate = m.player.focusedChild.downloadedSegment.bitrateBps
                end if
                else
                m.renditionAudioBitrate = m.streamBitrate
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONAUDIOBITRATE] = m.renditionAudioBitrate
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONNAME)
        if m.player <> invalid
            if m.player.streamingSegment <> invalid
                m.renditionName = m.player.streamingSegment.segUrl
            end if
        end if
         m.template.event.metrics[m.eventConfig.FLUXRENDITIONNAME] = m.renditionName
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONWIDTH)
        if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.width > 0
                m.renditionWidth = m.player.downloadedSegment.width
                else
                m.renditionWidth = m.playerWidth
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONWIDTH] = m.renditionWidth
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONHEIGHT)
     if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.height > 0
                m.renditionHeight = m.player.downloadedSegment.height
                else
                m.renditionHeight = m.playerHeight
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONHEIGHT] = m.renditionHeight
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXBITRATE)
    if m.player <> invalid
            if m.player.state <> invalid
                bitrate = m.renditionBitrate
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXBITRATE] = m.renditionBitrate
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXBANDWIDTH)
        m.template.event.metrics[m.eventConfig.FLUXBANDWIDTH] = m.networkBandwidth
    end if

    ' Time Since flux metrics    
        If checkIfFluxConfigured(m.eventConfig.FLUXTSBUFFERSTART)
        if m.bufferStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSBUFFERSTART] = m.bufferStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSBUFFERSTART] = getCurrentTimestampInMillis() - m.bufferStartTime
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSCONTENTBUFFERSTART)
        if m.TSBUFFERSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSCONTENTBUFFERSTART] = m.TSBUFFERSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSCONTENTBUFFERSTART] = getCurrentTimestampInMillis() - m.bufferStartTime
        end if
    end if
        If checkIfFluxConfigured(m.eventConfig.FLUXTSADBUFFERSTART)
        if m.adBufferStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADBUFFERSTART] = m.adBufferStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADBUFFERSTART] = getCurrentTimestampInMillis() - m.adBufferStartTime
        end if
        
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSTALLSTART)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSTALLSTART] = m.stallStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSTALLSTART] = getCurrentTimestampInMillis() - m.stallStartTime
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSCONTENTSTALLSTART)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSCONTENTSTALLSTART] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSCONTENTSTALLSTART] = getCurrentTimestampInMillis() - m.TSSTALLSTART
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSADSTALLSTART)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSTALLSTART] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSTALLSTART] = getCurrentTimestampInMillis() - m.TSADSTALLSTART
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTHEARTBEAT)
        if m.TSLASTHEARTBEAT = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = m.TSLASTHEARTBEAT
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = getCurrentTimestampInMillis() - m.TSLASTHEARTBEAT
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTADHEARTBEAT)
        if m.lastAdHeartbeatTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADHEARTBEAT] = m.lastAdHeartbeatTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADHEARTBEAT] = getCurrentTimestampInMillis() - m.lastAdHeartbeatTime
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTMILESTONE)
        if m.TSLASTMILESTONE = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTMILESTONE] = m.TSLASTMILESTONE
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTMILESTONE] = getCurrentTimestampInMillis() - m.TSLASTMILESTONE
        end if
    end if
        If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTADMILESTONE)
        
        if m.lastAdMilestoneTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADMILESTONE] = m.lastAdMilestoneTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADMILESTONE] = getCurrentTimestampInMillis() - m.lastAdMilestoneTime
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTAD)
        if m.lastAdTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTAD] = m.lastAdTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTAD] = getCurrentTimestampInMillis() - m.lastAdTime
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSPAUSED)
        if m.TSPAUSED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSPAUSED] = m.TSPAUSED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSPAUSED] = getCurrentTimestampInMillis() - m.TSPAUSED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSREQUESTED)
        if m.TSREQUESTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSREQUESTED] = m.TSREQUESTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSREQUESTED] = getCurrentTimestampInMillis() - m.TSREQUESTED
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFCONTENTREQUEST)
            m.template.event.metrics[m.eventConfig.FLUXNUMBEROFCONTENTREQUEST] = m.contentRequestCount
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFCONTENTPLAYS)
            m.template.event.metrics[m.eventConfig.FLUXNUMBEROFCONTENTPLAYS] = m.numberOfContentPlays
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXVIEWSTARTTIME)
        if m.TSREQUESTED = 0
            m.template.event.metrics[m.eventConfig.FLUXVIEWSTARTTIME] = m.TSREQUESTED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSTARTED)
        if m.TSSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSTARTED] = m.TSSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSTARTED] = getCurrentTimestampInMillis() - m.TSSTARTED
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT)
        if m.TSSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT] = m.TSSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONCONTENT] = getCurrentTimestampInMillis() - m.TSSTARTED
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATIONADS)
        if m.TSADSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONADS] = m.TSADSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATIONADS] = getCurrentTimestampInMillis() - m.TSADSTARTED
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSADREQUESTED)
        if m.TSADSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADREQUESTED] = m.TSADSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADREQUESTED] = getCurrentTimestampInMillis() - m.TSADSTARTED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSADBREAKSTART)
        if m.TSADBREAKSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADBREAKSTART] = m.TSADBREAKSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADBREAKSTART] = getCurrentTimestampInMillis() - m.TSADBREAKSTART
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSADSTARTED)
        if m.TSADIMPRESSION = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSTARTED] = m.TSADIMPRESSION
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSTARTED] = getCurrentTimestampInMillis() - m.TSADIMPRESSION
        end if
    end if
    
         If checkIfFluxConfigured(m.eventConfig.FLUXENGAGEMENTDURATION)
        if m.TSLOADED = 0
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATION] = m.TSLOADED
        else
            m.template.event.metrics[m.eventConfig.FLUXENGAGEMENTDURATION] = getCurrentTimestampInMillis() - m.TSLOADED
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSEEKSTART)
        if m.adSeekStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSADSEEKSTART] = m.adSeekStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSADSEEKSTART] = getCurrentTimestampInMillis() - m.adSeekStartTime
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSEEKSTART)
        if m.seekStartTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSEEKSTART] = m.seekStartTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSEEKSTART] = getCurrentTimestampInMillis() - m.seekStartTime
        end if
    end if
    
    
'    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATIONCONTENT)
'            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 
'    end if

    
    If checkIfFluxConfigured(m.eventConfig.FLUXCONTENTSESSIONSTART)
            m.template.event.metrics[m.eventConfig.FLUXCONTENTSESSIONSTART] = m.contentSessionStart
    end if

    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTRENDITIONCHANGE)
        if m.TSRENDITIONCHANGE = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTRENDITIONCHANGE] = m.TSRENDITIONCHANGE
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTRENDITIONCHANGE] = getCurrentTimestampInMillis() - m.TSRENDITIONCHANGE
        end if
    end if
    
        If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNT] = m.playbackStallCountAds + m.playbackStallCountContent
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTCONTENT] = m.playbackStallCountContent
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLCOUNTADS] = m.playbackStallCountAds
        end if
    end if
    
     If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATION)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATION] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATION] = m.playbackStallDurationContent + m.playbackStallDurationAds
        end if
    end if
    
         If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONCONTENT] = m.playbackStallDurationContent
        end if
    end if
    
         If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS)
        if m.TSADSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS] = m.TSADSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXPLAYBACKSTALLDURATIONADS] = m.playbackStallDurationContent
        end if
    end if
    
    
end function

' Sets playheadPosition
function playerMetrics()
    m.template.event.metrics = {}
    'Change this variable value to set whether fluxdata values should be sent with events or not
    callFlexMetrics = true
    if callFlexMetrics
        fluxMetricsData()
    else
'        playHead = 0
        If checkIfFluxConfigured(m.eventConfig.FLUXPLAYHEADUPDATE)
            if m.player <> invalid
                if m.player.currentTime <> invalid
                   m.playHead = m.player.currentTime
                end if
            end if
        end If
        m.template.event.metrics["playhead_position_sec"] = m.playHead
    end if
end function


' Player callback method for buffer status changes.
Sub OnBufferingStatusChange()
'    ? "DZ-PRINT: BUFFERING STATUS CHANGE"
'    if m.player.bufferingStatus<>invalid
'        m.bufferFillVal = m.player.bufferingStatus.percentage
'        if(m.bufferFillVal >= 95)
'        if m.bufferStarted
'            m.bufferStarted = false
'            playerMetrics()
'            If checkIfEventConfigured(m.eventConfig.EVENTBUFFEREND)
'                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFEREND))
'                 m.bufferEnd = true
'            End If
'            m.TSBUFFERSTART = getCurrentTimestampInMillis() - m.bufferStartTime
'            m.bufferDuration = m.bufferDuration +  m.TSBUFFERSTART
'            if m.videoType = "ad"
'                m.bufferDurationAds = m.bufferDurationAds +  m.TSBUFFERSTART
'                end if   
'            if m.videoType = "content"
'                m.bufferDurationContent = m.bufferDurationContent +  m.TSBUFFERSTART
'            end if   
''            If checkIfEventConfigured(m.eventConfig.EVENTSTALLEND)
''                wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLEND))
''                if m.videoType = "content"
''
''                m.playbackStallCountContent = m.playbackStallCountContent + 1
''                m.playbackStallDurationContent = m.playbackStallDurationContent + (getCurrentTimestampInMillis() - m.bufferStartTime)
''                end if
''                
''                if m.videoType = "ad"
''                m.playbackStallCountAds = m.playbackStallCountAds + 1
''                m.playbackStallDurationAds = m.playbackStallDurationAds + (getCurrentTimestampInMillis() - m.bufferStartTime)
''                end if   
''            End If
'          end if
'        end if
'   end if
End Sub

' Function to set resume flag
function setResume()
    m.playerResume = true
end function

sub setState()
    m.prevState = m.currentState
    m.currentState = m.player.playerState
    ? "DZ-PRINT: STATES:"; m.prevState;"->";m.currentState
end sub


' Seek methods
Sub onSeek()
? "DZ-PRINT: SEEK START"
        m.seekStartPoint =  m.player.timeChanged.time * 1000
        If checkIfEventConfigured(m.eventConfig.EVENTSEEKSTART)
           m.seekStartTime = getCurrentTimestampInMillis() 
           m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.seekStartPoint
           wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKSTART))
           m.template.event.attributes = {}
        End If  
end sub

Sub onSeeked()
    ? "DZ-PRINT: SEEK END"
         If checkIfEventConfigured(m.eventConfig.EVENTSEEKEND)
'            m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.seekStartPoint
            m.template.event.attributes[m.eventConfig.METASEEKENDPOINT] = m.player.timeChanged.time * 1000
            wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKEND))
            m.template.event.attributes = {}
         End If   
end sub

sub OnContentLoaded()
                                                
                   If m.playbackTimer = invalid
                        m.playbackTimer = CreateObject("roTimespan")
                   end if
                   m.heartCount = 1

                    If checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
                     wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED))
                    End If
                    m.cnf = m.player.callFunc(m.bitmovinFunctions.GET_CONFIG, invalid)       
                    m.metatitle = m.cnf.source.title 
                    if m.player.callFunc(m.bitmovinFunctions.IS_LIVE, invalid)
                    m.videoType = "content"
                    m.streamingType = "Live"
                    end if        
end sub

' Player state change callback method.
sub OnVideoPlayerStateChange()
    playerMetrics()  
    
    'Setup State
    if m.player.playerState = "setup"
        setState()
                    
     end if
    
    'Ready State
    if m.player.playerState = "ready"
    setState()
                    m.playStateTimer.control = "start"
                    m.quartileTimer.control = "start"
                    m.dataTimer.control = "start"
                    callToSetNoOfVideos()
                    playerMetrics()   
                    m.defaultMute = m.player.mute                 
                    m.TSREQUESTED = getCurrentTimestampInMillis()
                    m.TSLASTADHEARTBEAT = 0
                    m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = 0
                    m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 0
                    m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = 0
                    m.playbackCompleteTriggered = false
                    m.startupTimer = CreateObject("roTimespan")                
    
    If checkIfEventConfigured(m.eventConfig.EVENTPLAYERREADY)
       wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYERREADY))
    End If
    
    If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
       wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST))       
       m.playRequestTriggered = true                 
    End If
    m.contentRequestCount = m.contentRequestCount + 1
    m.contentSessionStart = getCurrentTimestampInMillis()
    m.TSREQUESTED = getCurrentTimestampInMillis()                
                       
    
'         m.contentSessionId = m.device.GetRandomUUID()
'        If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
''            startupDuration = m.startupTimer.TotalMilliseconds()
''            m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
'            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST)) 
'            m.playRequestTriggered = true
'                startupDuration = m.startupTimer.TotalMilliseconds()
'                m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
'                m.heartCount = 1
'
'                m.TSSTARTED = getCurrentTimestampInMillis()  
'       If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKSTART) and  m.firstFrameTriggered = false
'       
'       m.contentSessionId = m.device.GetRandomUUID() + "-1"
'             if m.playbackTimer = invalid
'                    m.playbackTimer = CreateObject("roTimespan")
'             end if
'
'                If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKSTART)
'                    wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKSTART))
'                    m.firstFrameTriggered = true
'                End If
'                m.template.event.attributes = {}
'        End If
                     
'       startupDuration = m.startupTimer.TotalMilliseconds()
'       m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
'        m.numberOfContentPlays = m.numberOfContentPlays + 1

        
     end if
    
    'Playing State
    if m.player.playerState = "playing"
    setState()
    m.playStateTimer.ObserveField("fire","playStateTimerFunction")
    if m.player.callFunc(m.bitmovinFunctions.IS_LIVE, invalid)
                    m.videoType = "content"
                    m.streamingType = "Live"
    end if        
    if m.prevState = "ready" or m.firstFrameTriggered = false
'    If checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
'    wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED))
'    End If
'    m.cnf = m.player.callFunc(m.bitmovinFunctions.GET_CONFIG, invalid) 
    m.TSSTARTED = getCurrentTimestampInMillis()  
    m.numberOfContentPlays = m.numberOfContentPlays + 1
    m.contentSessionId = m.device.GetRandomUUID()
    m.contentsMilestones = m.top.contentMilestones
    m.contentsQV = m.top.contentQV
       If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKSTART) and  m.firstFrameTriggered = false
             if m.playbackTimer = invalid
                    m.playbackTimer = CreateObject("roTimespan")
             end if
             If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKSTART)
                if m.player.focusedChild.timeToStartStreaming <> invalid
                startupDurationContent = m.player.focusedChild.timeToStartStreaming * 1000
                startupDuration = m.startupTimer.TotalMilliseconds()
                m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
                m.template.event.attributes[m.eventConfig.METASTARTUPDURATIONCONTENT] = startupDurationContent
                end if
                    wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKSTART))
                    m.firstFrameTriggered = true
             End If
             
                m.template.event.attributes = {}
        End If
    end if
    
    if m.prevState = "paused"
        m.videoPaused = false
            if m.pauseTimer <> invalid
                m.pausedTime = m.pauseTimer.TotalMilliseconds()
                if m.videoType = "content"
                m.pauseDuration = m.pauseDuration + m.pausedTime
                m.pauseTimer = invalid
                end if
                if m.videoType = "ad"
                m.pauseDurationAds = m.pauseDurationAds + m.pausedTime
                m.pauseTimer = invalid
                end if
            end if 
        If checkIfEventConfigured(m.eventConfig.EVENTRESUME)
            wsSend(getMessageTemplate(m.eventConfig.EVENTRESUME))
        End If 
    end if   
    if m.prevState = "stalling"
        if m.bufferStarted 
         m.bufferStarted = false
            m.TSBUFFERSTART = getCurrentTimestampInMillis() - m.bufferStartTime
            m.bufferDuration = m.bufferDuration +  m.TSBUFFERSTART
            if m.videoType = "ad"
                m.bufferDurationAds = m.bufferDurationAds +  m.TSBUFFERSTART
            end if   
            if m.videoType = "content"
                m.bufferDurationContent = m.bufferDurationContent +  m.TSBUFFERSTART
            end if  
            If checkIfEventConfigured(m.eventConfig.EVENTBUFFEREND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFEREND))         
            End If
        m.bufferEnd = true
    end if
    if m.stallStarted
        m.stallStarted = false
           If checkIfEventConfigured(m.eventConfig.EVENTSTALLEND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLEND))
           End If
           if m.videoType = "content"
                m.playbackStallCountContent = m.playbackStallCountContent + 1
                m.playbackStallDurationContent = m.playbackStallDurationContent + (getCurrentTimestampInMillis() - m.bufferStartTime)
           end if
           if m.videoType = "ad"
                m.playbackStallCountAds = m.playbackStallCountAds + 1
                m.playbackStallDurationAds = m.playbackStallDurationAds + (getCurrentTimestampInMillis() - m.bufferStartTime)
           end if   
    end if
    
    
    end if
    
    if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN)) 
        end If   
        
        If checkIfEventConfigured(m.eventConfig.EVENTPLAY)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
        End If
    
        If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If    
    
    end if
    
    
    if m.player.playerState = "ready"
    
    'buffer and stall
    if m.stallStarted and m.prevState = "playing"
                m.stallStarted = false
           If checkIfEventConfigured(m.eventConfig.EVENTSTALLEND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLEND))
           End If
           if m.videoType = "content"
                m.playbackStallCountContent = m.playbackStallCountContent + 1
                m.playbackStallDurationContent = m.playbackStallDurationContent + (getCurrentTimestampInMillis() - m.bufferStartTime)
           end if
           if m.videoType = "ad"
                m.playbackStallCountAds = m.playbackStallCountAds + 1
                m.playbackStallDurationAds = m.playbackStallDurationAds + (getCurrentTimestampInMillis() - m.bufferStartTime)
           end if   
    end if
    if m.bufferStarted 
        m.bufferStarted = false
            m.TSBUFFERSTART = getCurrentTimestampInMillis() - m.bufferStartTime
            m.bufferDuration = m.bufferDuration +  m.TSBUFFERSTART
            if m.videoType = "ad"
                m.bufferDurationAds = m.bufferDurationAds +  m.TSBUFFERSTART
            end if   
            if m.videoType = "content"
                m.bufferDurationContent = m.bufferDurationContent +  m.TSBUFFERSTART
            end if  
            If checkIfEventConfigured(m.eventConfig.EVENTBUFFEREND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFEREND))         
            End If
        m.bufferEnd = true
    end if
    if m.prevState = "paused"
        if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN)) 
        end If   
        
        If checkIfEventConfigured(m.eventConfig.EVENTPLAY)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
        End If
        
        if m.pauseTimer <> invalid
            m.pausedTime = m.pauseTimer.TotalMilliseconds()
            
            if m.videoType = "content"
                m.pauseDuration = m.pauseDuration + m.pausedTime
                m.pauseTimer = invalid
                end if
            if m.videoType = "ad"
                m.pauseDurationAds = m.pauseDurationAds + m.pausedTime
                m.pauseTimer = invalid
            end if
        end if 
        
        If checkIfEventConfigured(m.eventConfig.EVENTRESUME)
            wsSend(getMessageTemplate(m.eventConfig.EVENTRESUME))
        End If 
        
        If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If
        
        m.resumePoint = m.player.position
        m.playCounter = m.playCounter + 1
    end if  
    
    if m.prevState = "ready"
        If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST)) 
            m.playRequestTriggered = true
        End If
            m.heartCount = 1
'            m.firstFrameTriggered = true 
            m.TSSTARTED = getCurrentTimestampInMillis() 
            m.template.event.attributes = {}
            m.numberOfContentPlays = m.numberOfContentPlays + 1
            m.contentRequestCount = m.contentRequestCount + 1
            m.contentSessionStart = getCurrentTimestampInMillis()
      
      
      '============= Playback Start=======================
       If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKSTART) and  m.firstFrameTriggered = false
                startupDurationContent = m.player.focusedChild.timeToStartStreaming * 1000
                startupDuration = m.startupTimer.TotalMilliseconds()
                m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
                m.template.event.attributes[m.eventConfig.METASTARTUPDURATIONCONTENT] = startupDurationContent
                If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKSTART)
                ? "DZ-PRINT: PSRART 1"
                    wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKSTART))
                    m.firstFrameTriggered = true
                End If
                m.template.event.attributes = {}
        End If
        m.numberOfContentPlays = m.numberOfContentPlays + 1
        m.TSSTARTED = getCurrentTimestampInMillis()
        If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If
    end if          
        
        if m.playCounter >1
            setResume()
        end if
        
 if m.playerResume
            m.resumePoint = m.player.position
'   if  Abs(m.pausePoint - m.resumePoint) > 1
'            m.pauseTimer = CreateObject("roTimespan")
'             m.pauseTimer.Mark()
'        If checkIfEventConfigured(m.eventConfig.EVENTSEEKSTART)
'           if m.videoType = "ad"
'               m.adSeekStartTime = getCurrentTimestampInMillis()
'               m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.adSeekStartTime
'               wsSend(getMessageTemplate(m.eventConfig.EVENTADSEEKSTART))
'               m.template.event.attributes = {}
'           end if
'        End If        
'        If checkIfEventConfigured(m.eventConfig.EVENTSEEKSTART)
'           m.seekStartTime = getCurrentTimestampInMillis()
'           m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.seekStartTime
'           wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKSTART))
'           m.template.event.attributes = {}
'        End If  
'
'        If checkIfEventConfigured(m.eventConfig.EVENTSEEKEND)
''                ? "PAUSE POINT:"; m.pausePoint
'                m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.pausePoint * 1000
'                m.template.event.attributes[m.eventConfig.METASEEKENDPOINT] = m.player.position * 1000
'                wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKEND))
'                m.template.event.attributes = {}
'        End If   
        
'        print ("SEEK EVENT")
'        print "PAUSE TIME"; m.pausePoint; "STARTED:"; m.resumePoint;
'   end if
'       if m.pausePoint > m.resumePoint 
'         If checkIfEventConfigured(m.eventConfig.EVENTSEEKEND)
'                m.template.event.attributes[m.eventConfig.METASEEKSTART] = m.pausePoint
'                m.template.event.attributes[m.eventConfig.METASEEKSTART] = m.startPoint
'                wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKEND))
'                m.template.event.attributes = {}
'            End If
'       end if
            playerMetrics()
        
        else
            setResume()
'            If checkIfEventConfigured(m.eventConfig.EVENTPLAY)
'                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
'            End If
            
            
'      If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
''         startupDuration = m.startupTimer.TotalMilliseconds()
''         m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
'          wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST)) 
'          m.playRequestTriggered = true
'               if m.playbackTimer = invalid
'                   m.playbackTimer = CreateObject("roTimespan")
'               end if
''              if m.playbackTimer.TotalMilliseconds() <> 0
'                
'                
''              end if
''        startupDuration = m.startupTimer.TotalMilliseconds()
''         m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
'         m.heartCount = 1
'         m.firstFrameTriggered = true 
''        m.playRequestTriggered = false
'                
''        m.delayTimer.control = "start"
'         m.TSSTARTED = getCurrentTimestampInMillis()  
'         (getMessageTemplate(m.eventConfig.EVENTPLAYBACKSTART))
'         m.template.event.attributes = {}                  
'     End If
'            m.numberOfContentPlays = m.numberOfContentPlays + 1
'            m.contentRequestCount = m.contentRequestCount + 1
'            m.contentSessionStart = getCurrentTimestampInMillis()
                    
'            If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKSTART) and  m.firstFrameTriggered = false
'                if m.playbackTimer = invalid
'                    m.playbackTimer = CreateObject("roTimespan")
'                end if
'                startupDuration = m.startupTimer.TotalMilliseconds()
'                m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
''                if m.playbackTimer.TotalMilliseconds() <> 0
'                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKSTART))
''                end if
'                m.firstFrameTriggered = true
'                m.template.event.attributes = {}
'            End If
'            startupDuration = m.startupTimer.TotalMilliseconds()
'            m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
'        end if
         
'        If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST) and m.playRequestTriggered = false
'            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST)) 
'            m.playRequestTriggered = true                      
'            '====== REMOVE THIS LATER=========
'        if m.playbackTimer = invalid
'        m.playbackTimer = CreateObject("roTimespan")
'        end if
''        startupDuration = m.startupTimer.TotalMilliseconds()
''        m.template.event.attributes[m.eventConfig.METATOTALSTARTUPDURATION] = startupDuration
'        m.template.event.attributes = {}
'        m.heartCount = 1
'        m.firstFrameTriggered = true 
'       ' m.playRequestTriggered = false
'        m.TSSTARTED = getCurrentTimestampInMillis()
'        m.delayTimer.control = "start"
''        wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKSTART))                   
'            '==============================
'        End If
        
'        If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKSTART) and m.firstFrameTriggered = false
'        if m.playbackTimer = invalid
'         m.playbackTimer = CreateObject("roTimespan")
'        end if
'        wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKSTART))
'        m.heartCount = 1
'        m.firstFrameTriggered = true 
'        m.playRequestTriggered = false
'      end if
      end if
             
     else if m.player.PlayerState = "paused"
        setState()
        m.videoPaused = true
        m.pausePoint = m.player.timeChanged.time
        m.TSPAUSED = getCurrentTimestampInMillis()
        m.pauseTimer = CreateObject("roTimespan")
        m.pauseTimer.Mark()

        
        If checkIfEventConfigured(m.eventConfig.EVENTPAUSE)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPAUSE))
        End If
        
'        ? "DZ-PRINT: TIMECHANGED = "; m.player.timeChanged
'        ? "DZ-PRINT: DOWNLOAD FINISHED = "; m.player.downloadFinished
'        ? "DZ-PRINT: IMPRESSION = "; m.player.impression
'        ? "DZ-PRINT: STREAM INFO = "; m.player.focusedChild.streamInfo
'        ? "DZ-PRINT: STREAM DLOADEDSEGMENT = "; m.player.focusedChild.downloadedSegment
'        ? "DZ-PRINT: STREAM CONTENT = "; m.player.focusedChild.content.change
'        ? "DZ-PRINT: STREAM PLAYSTARTINFO = "; m.player.focusedChild.playStartInfo
'        ? "DZ-PRINT: ERROR = "; m.player.error
'        ? "DZ-PRINT: FOCUSEDCHILD = "; m.player.focusedChild
'        ? "DZ-PRINT: BITMOVIN FIELDS = "; m.player.bitmovinFields
'        ? "DZ-PRINT: CONFIG:"; m.cnf.source.title
        
     else if m.player.playerState = "stalling"
        setState()
        if m.prevState = "ready" or m.prevState = invalid
        m.bufferStarted = true
        m.bufferEnd = false
        m.bufferStartTime = getCurrentTimestampInMillis()
        
            If checkIfEventConfigured(m.eventConfig.EVENTBUFFERSTART)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERSTART))
            End If
            If checkIfEventConfigured(m.eventConfig.EVENTBUFFERING)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERING))
            End If
        
            if m.videoType = "ad"
                m.adBufferStartTime = getCurrentTimestampInMillis()
                m.TSADSTALLSTART = getCurrentTimestampInMillis()
            end if
            if m.videoType = "content"
                m.TSBUFFERSTART = getCurrentTimestampInMillis()
                m.TSSTALLSTART = getCurrentTimestampInMillis()
            end if

        else if m.prevState = "playing"
        m.stallStarted = true
        m.bufferStarted = true
        m.bufferEnd = false
            
            if m.player.currentTime <> invalid
                m.TSSTALLSTART = getCurrentTimestampInMillis()
                m.stallStartTime = getCurrentTimestampInMillis()
            end if
            if m.videoType = "ad"
                m.TSADSTALLSTART = getCurrentTimestampInMillis()
                m.adBufferStartTime = getCurrentTimestampInMillis()
            end if
            if m.videoType = "content"
                m.TSSTALLSTART = getCurrentTimestampInMillis()    
            end if
            
            If checkIfEventConfigured(m.eventConfig.EVENTSTALLSTART)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLSTART))
            End If
        
            If checkIfEventConfigured(m.eventConfig.EVENTBUFFERSTART)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERSTART))
            End If
            If checkIfEventConfigured(m.eventConfig.EVENTBUFFERING)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERING))
            End If
            
         end if
       
        
     else if m.player.playerState = "error"
        setState()
         m.numberOfErrors = m.numberOfErrors + 1
        
        if m.videoType = "content"
        m.numberOfErrorsContent = m.numberOfErrorsContent + 1
        end if
        
        If checkIfEventConfigured(m.eventConfig.EVENTERROR)
          m.template.event.attributes[m.eventConfig.METAERRORCODE] = m.player.focusedChild.errorCode
          m.template.event.attributes[m.eventConfig.METAERRORMSG] = m.player.focusedChild.errorCode
          wsSend(getMessageTemplate(m.eventConfig.EVENTERROR))    
          m.template.event.attributes = {}  
        end if  
        
    else if m.player.playerState = "stopped"
        setState()
'         m.firstFrameTriggered = false
'         m.playRequestTriggered = false
'         m.template.custom = {}
'         m.playingDuration = 0
'         m.playbackDurationContent = 0
'         m.TSLASTADHEARTBEAT = 0
        
    else if m.player.playerState = "finished"
        setState()
        if m.videoType = "content"
        m.quartileTimer.control = "stop"
        m.dataTimer.control = "stop"
        m.player.callFunc(m.bitmovinFunctions.DESTROY, invalid)
        m.firstFrameTriggered = false
        m.playbackCompleteTriggered = true
        m.customPlayerMeta = {}
        m.playbackTimer = Invalid              
        m.playRequestTriggered = false
        m.template.custom = {}
        m.playingDuration = 0
        m.playbackDurationContent = 0
        m.TSLASTADHEARTBEAT = 0
    end if 
    If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKCOMPLETE) and m.playbackCompleteTriggered = false
         wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKCOMPLETE))
    End If
    
    end if       
end Sub

' Runs at specific interval to check is milestone position is reached
sub quartileTimerFunction()
    videoRunning = false
    
        if m.player.playerState = "playing"
                videoRunning = true
        end if
    if videoRunning
'        ? "DZ-PRINT: VIDEO RUNNING CONTENT TYPE =" m.videoType
        if m.videoType = "content"
        if m.player <> invalid and m.player.focusedChild <> invalid and m.player.currentTime <> invalid 
            if m.player.focusedChild.duration <> invalid and not m.player.callFunc(m.bitmovinFunctions.IS_LIVE, invalid) 
                playerPosition = m.player.currentTime
                playerDuration = m.player.focusedChild.duration
'                ? "DZ-PRINT: MILESTONE = ";playerPosition; "/"; playerDuration
                
                if playerDuration > 0 and playerPosition > 0 and m.contentsMilestones.Count() > 0
                    m.template.event.attributes = {}
                    if playerPosition >= (playerDuration * (m.contentsMilestones[0]/100))
                            
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                                If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = m.contentsMilestones[0]/100
                                end if
                                m.milestonePercent = m.contentsMilestones[0]/100
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))

                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.template.event.attributes = {}
                                if m.contentsMilestones.count() > 1
                                     m.contentsMilestones.Shift()
                                else
                                    m.contentsMilestones.Clear()
                                end if
                            End If
                        end if
                    
                    
                    
                    
'                        if playerPosition >= (playerDuration * 0.95) and m.fired95 = false
'                            
'                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
'                                If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
'                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.95
'                                end if
'                                m.milestonePercent = 0.95
'                                playerMetrics()
'                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
'                                m.fired95 = true
'                                m.template.event.attributes = {}
'                            End If
''                        end if
'                        else if playerPosition >= (playerDuration * 0.9) and m.fired90 = false
'                        if playerPosition < ((playerDuration * 0.9)+2) and m.fired90 = false
'                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
'                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
'                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.9
'                                end if
'                                m.milestonePercent = 0.9
'                                playerMetrics()
'                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
'                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
'                                m.fired90 = true
'                                m.template.event.attributes = {}
'                            End If
'                        end if
'                    else if playerPosition >= (playerDuration * 0.75)  and m.fired75 = false
'                        if playerPosition < ((playerDuration * 0.75)+1) and m.fired75 = false
'                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
'                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
'                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.75
'                                end if
'                                m.milestonePercent = 0.75
'                                playerMetrics()
'                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
'                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
'                                m.fired75 = true
'                                m.template.event.attributes = {}
'                            End If
'                        end if
'                    else if playerPosition >= (playerDuration * 0.50) and m.fired50 = false
'                        if playerPosition < ((playerDuration * 0.50)+1) and m.fired50 = false
'                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
'                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
'                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.50
'                                end if
'                                m.milestonePercent = 0.50
'                                playerMetrics()
'                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
'                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
'                                m.fired50 = true
'                                m.template.event.attributes = {}
'                            End If
'                        end if
'                        else if playerPosition >= (playerDuration * 0.25) and m.fired25 = false
'                        if playerPosition < ((playerDuration * 0.25)+1) and m.fired25 = false
'                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
'                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
'                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.25
'                            end if
'                                m.milestonePercent = 0.25
'                                playerMetrics()
'                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
'                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
'                                m.fired25 = true
'                                m.template.event.attributes = {}
'                            End If
'                        end if
'                    else if playerPosition >= (playerDuration * 0.11) and m.fired10 = false
'                         
'                        if playerPosition <= ((playerDuration * 0.11)+1) and m.fired10 = false
'                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
'                            
'                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
'                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.10
'                            end if
'                                m.milestonePercent = 0.10
'                                playerMetrics()
'                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
'                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
'                                m.fired10 = true
'                                m.template.event.attributes = {}
'                            End If
'                        end if
'                    end if
                end if
            end if
        end if
    else if m.videoType = "ad"
    if m.player.timeChanged <> invalid
    ? "MOMO AD STARTED = "; m.player.adStarted
    ? "MOMO TIME CHANGED = "; m.player.timeChanged
    ? "MOMO FOCUSED CHILD = "; m.player.focusedChild.focusedChild
    m.adTime = m.player.timeChanged.time
    if m.adTime <> invalid
        if m.oldTimeAd <> m.adTime
            if m.videoPaused = true
            if m.pauseTimer <> invalid
            m.pausedTime = m.pauseTimer.TotalMilliseconds()
            ?"MOMO AD RESUME!!!!!!!!!!!!!!!!!!!!!!"
            if m.videoType = "ad"
                m.pauseDurationAds = m.pauseDurationAds + m.pausedTime
                m.pauseTimer = invalid
             end if
            m.videoPaused = false
          if checkIfEventConfigured(m.eventConfig.EVENTPLAYBTN)
          wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBTN)) 
          end If
          if checkIfEventConfigured(m.eventConfig.EVENTADRESUME)
          wsSend(getMessageTemplate("resume")) 
          end If
          If checkIfEventConfigured(m.eventConfig.EVENTPLAY)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
          End If
          If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If
            end if
            end if
            m.playbackDuration = m.playbackDuration + 1 '(m.adTime - m.oldTimeAd) 
            m.adMilestoneTime = m.adMilestoneTime + 1 'm.adTime
            m.oldTimeAd = m.adTime
            fluxMetricsData()
            else if m.oldTimeAd =  m.adTime
            if m.videoPaused = false
                    m.videoPaused = true
                     m.pausePoint = m.player.timeChanged.time
                        m.TSPAUSED = getCurrentTimestampInMillis()
                     m.pauseTimer = CreateObject("roTimespan")
                    m.pauseTimer.Mark()
        
'        If checkIfEventConfigured(m.eventConfig.EVENTPAUSE)
'            wsSend(getMessageTemplate(m.eventConfig.EVENTPAUSE))
'        End If
            ? "MOMO AD PAUSED!!!!!!!!!!!"
            end if
            
    end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTADMILESTONE)
        
        if m.lastAdMilestoneTime = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADMILESTONE] = m.lastAdMilestoneTime
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTADMILESTONE] = getCurrentTimestampInMillis() - m.lastAdMilestoneTime
        end if
    end if
    end if
    ? "DZ-PRINT: AD MILESTONE TIME = "; m.adMilestoneTime
    ? "DZ-PRINT: AD DURATION = "; m.playbackDuration
    
    if m.adsQV.Count() > 0
        ? "MOMO ADS QV = "; m.adsQV[0]
        ? "MOMO AD TIME = "; m.adMilestoneTime
        if m.adMilestoneTime >= m.adsQV[0] 
        m.template.event.attributes["qualified_view_sec"] = m.adsQV[0]
        wsSend(getMessageTemplate(m.eventConfig.EVENTQVIEW))
        m.template.event.attributes = {}
            if m.adsQV.count() > 1
                m.adsQV.Shift()
            else
                m.adsQV.Clear()
            end if
            ? "MOMO ADQV TRIGGERED!!!!!! = "; m.adsQV
        end if
      end if
    
    
    
    
    if m.adDuration <> invalid and m.adTime <> invalid and m.adsMilestones.Count() > 0
    
    ? "MOMO ADS MILESTONES = "; m.adsMilestones
    if m.adMilestoneTime <> invalid and m.adDuration <> invalid and m.adsMilestones[0]<> invalid
    if m.adMilestoneTime / m.adDuration > (m.adsMilestones[0]/100)
    m.template.event.attributes["milestone_percent"] = m.adsMilestones[0]/100
    wsSend(getMessageTemplate("milestone"))
    m.template.event.attributes = {}
    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
    ? "MOMO ADMILESTONE TRIGGERED!!!!!! = "; m.adsMilestones[0]
    if m.adsMilestones.Count() > 1
                m.adsMilestones.Shift()
            else
                m.adsMilestones.Clear()
            end if
    end if
    end if
    
'    if m.adMilestoneTime / m.adDuration > 0.1 and m.adFired10 = false 
'    m.template.event.attributes["milestonePercent"] = 0.10
'    wsSend(getMessageTemplate("milestone"))
'    m.adFired10 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'    if m.adMilestoneTime / m.adDuration > 0.25 and m.adFired25 = false 
'    m.template.event.attributes["milestonePercent"] = 0.25
'    wsSend(getMessageTemplate("milestone")) 
'    m.adFired25 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'    if m.adMilestoneTime / m.adDuration > 0.50 and m.adFired50 = false 
'    m.template.event.attributes["milestonePercent"] = 0.50
'    wsSend(getMessageTemplate("milestone"))
'    m.adFired50 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'     
'    end if
'    if m.adMilestoneTime / m.adDuration > 0.6 and m.adFired75 = false
'    m.template.event.attributes["milestonePercent"] = 0.75 
'    wsSend(getMessageTemplate("milestone"))  
'    m.adFired75 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'    if m.adMilestoneTime / m.adDuration > 0.7 and m.adFired90 = false 
'    m.template.event.attributes["milestonePercent"] = 0.90
'    wsSend(getMessageTemplate("milestone"))
'    m.adFired90 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
'    if m.adMilestoneTime / m.adDuration = 1 or m.adMilestoneTime / m.adDuration > 1 and m.adFired95 = false 
'    m.template.event.attributes["milestonePercent"] = 0.95
'    wsSend(getMessageTemplate("milestone"))
'    m.adFired95 = true
'    m.template.event.attributes = {}
'    m.lastAdMilestoneTime = getCurrentTimestampInMillis()
'    end if
end if

if m.top.events.interval <> invalid

if m.adTime <> invalid 'and m.adTime = 1
'if m.oldTimeAd <> m.adTime
'm.playbackDuration = m.playbackDuration + 1 '(m.adTime - m.oldTimeAd) 
'm.oldTimeAd = m.adTime
'm.adMilestoneTime = m.adMilestoneTime + 1 'm.adTime
'fluxMetricsData()
'end if
'else if m.adTime > 0
'm.adTime = m.adTime + 1
'm.playbackDuration = m.playbackDuration + 1
end if

        If (m.top.events.interval/1000) - (m.adTime - m.oldAdTime) = 0 
            wsSend(getMessageTemplate("Heartbeat"))
            m.oldAdTime = m.adTime
            m.lastAdHeartbeatTime = getCurrentTimestampInMillis()
        end if

    end if  
   end if
        
 end if
end sub

' Runs at specific interval to update the data
sub dataTimerFunction()
    ? "DZ-PRINT: DATA TIMER FUNC"
        videoRunning = false
        if m.player.playerState = "playing"
                videoRunning = true
        end if
  
    if m.player <> invalid
        'is video running?
        'is video Muted
        
        if videoRunning
           videoMute = false
           if m.mute <> videoMute
            m.mute = videoMute
            if m.mute
                If checkIfEventConfigured(m.eventConfig.EVENTMUTE)
                  playerMetrics()
'                  wsSend(getMessageTemplate(m.eventConfig.EVENTMUTE))
                End If
            else
                If checkIfEventConfigured(m.eventConfig.EVENTUNMUTE)
                  playerMetrics()
'                  wsSend(getMessageTemplate(m.eventConfig.EVENTUNMUTE))
                End If
            end if
        end if
        
       'Collect stream metadata and info
       
       'timedMetadata
'       ? "DZ-PRINT: TIMED METADATA = "; m.player.focusedChild.timedMetaDataSelectionKeys
'       m.player.focusedChild.timedMetaDataSelectionKeys = ["*"]
'       if m.player.timedMetaData <> invalid
'            ba = CreateObject("roByteArray")
'            ba.FromHExString(m.player.timedMetaData.PRIV)
'            strg = ba.ToAsciiString()
'        end if
       
       'stream info
       if m.player.focusedChild <> invalid
       if m.player.focusedChild.streamInfo <> invalid
       if m.player.focusedChild.streamInfo.measuredBitrate <> invalid
       
       m.networkBandwidth = m.player.focusedChild.streamInfo.measuredBitrate / 1000
       end if 
       end if
       end if
       
       ' downloaded segment
'       if m.player.focusedChild.downloadedSegment <> invalid
'       
'       end if
       
       'streaming segment
'       if m.player.focusedChild.downloadedSegment <> invalid 
'           if m.player.focusedChild.downloadedSegment.bitrateBps <> invalid or m.player.focusedChild.downloadedSegment.bitrateBps <> 0 
'           m.renditionBitrate = m.player.focusedChild.downloadedSegment.bitrateBps
'           else if m.player.streamBitrate <> invalid or m.player.streamBitrate <> 0 
'           m.renditionBitrate = m.player.streamBitrate
'           else if m.player.streamInfo.streamBitrate <> invalid or m.player.streamInfo.streamBitrate <> 0 
'           m.streamBitrate = m.player.streamInfo.streamBitrate
'       end if
'
'       end if
        if m.player.streamingSegment <> invalid
            if m.player.focusedChild.downloadedSegment.bitrateBps <> invalid
                 streamB = m.player.focusedChild.downloadedSegment.bitrateBps
                
                 m.renditionBitrate  = streamB
                 m.networkBandwidth = m.renditionBitrate / 1000
                 if m.streamBitrate <> streamB
                 m.streamBitrate = streamB
                 playerMetrics()
                   if streamB < m.streamBitrate
                   m.template.event.attributes[m.eventConfig.METAABSSHIFT] = "up"
                   end if
                   if streamB > m.streamBitrate
                   m.template.event.attributes[m.eventConfig.METAABSSHIFT] = "down"
                   end if
                 m.streamBitrate = streamB
                 If checkIfEventConfigured(m.eventConfig.EVENTBITRATECHANGE)
                 playerMetrics()
                 wsSend(getMessageTemplate(m.eventConfig.EVENTBITRATECHANGE))
                 end if
                 If checkIfEventConfigured(m.eventConfig.EVENTRENDITIONCHANGE) and m.currentState = "playing"
                 playerMetrics()
                 wsSend(getMessageTemplate(m.eventConfig.EVENTRENDITIONCHANGE))
                 m.TSRENDITIONCHANGE = getCurrentTimestampInMillis()
                 m.template.event.attributes = {}
                 end if
                 end if
            end if
        end if
    end if
    end if
end sub

' Checks when player is ready to play
function playStateTimerFunction()
'    ? "DZ-PRINT: PLAY STATE TIMER"
    fluxMetricsData()
    if m.player.playerState  = "playing" and m.videoType = "ad"
        if m.player.timeChanged <> invalid
        m.adTime = m.player.timeChanged.time
        
'                   callToSetNoOfVideos()
'                    playerMetrics()                    
'                    m.TSREQUESTED = getCurrentTimestampInMillis()
'                    m.TSLASTADHEARTBEAT = 0
'                    m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = 0
'                    m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATIONCONTENT] = 0
'                    m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = 0
'                    If checkIfEventConfigured(m.eventConfig.EVENTPLAYERREADY)
'                     wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYERREADY))
'                    End If
'                    If checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
'                     wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED))
'                    End If
'
''                    If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
''                        wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST))       
''                        m.playRequestTriggered = true                 
''                    End If
'                    m.playbackCompleteTriggered = false
'                    m.startupTimer = CreateObject("roTimespan")
'                    m.playbackTimer = CreateObject("roTimespan")
end if
    end if
end function

'----------------------- Collector Starts --------------------------------

' Method to get the message template

Function getMessageTemplate(event as String) as object
    m.eventCount = m.eventCount + 1
    tenths = 00
    if m.eventCount > 9999
    tenths = tenths + 1
    eventCount = 1
    end if
    evntCount$ = m.eventCount.ToStr()
    if len(evntCount$) < 4
    eventCount$ = String((4 - len(evntCount$)), "0") + evntCount$
    end if
    If tenths < 10
    tenth$ = "0"+ tenths.toStr()
    else 
    tenth$ = tenths.toStr()
    end if
    m.template.user_details.app_session_id = getSessionData()
    m.template.event.metrics[m.eventConfig.METAEVENTCOUNT] = m.eventCount
    m.template.event_id = m.template.user_details.app_session_id+"_"+tenth$+"."+eventCount$
    
    m.template.custom.Append(m.customSessionMeta)
    m.template.custom.Append(m.customPlayerMeta)
    m.template.customer_code = m.responseBody.customer_code
    m.template.connector_list= m.responseBody.connector_list
    m.template.configuration_id = m.responseBody.configuration_id
    m.template.event.type = event
    m.template.event.timestamp = getCurrentTimestampInMillis()
'    m.template.ops_metadata["client_ts_millis"] = m.template.event.timestamp
    m.template.device.id = getUniqueDeviceId()
    m.template.user_details.is_anonymous = false
'    m.template.user_details.app_session_id = getSessionData()
    getGeoMeta()
'    PLAYER INFO

        if m.player <> invalid
            
            If checkIfMetaConfigured(m.eventConfig.METAPLAYERNAME)
            mm.template.player["player_name"] = "Bitmovin Player"
            end if            
            If checkIfMetaConfigured(m.eventConfig.METAPLAYERVERSION)
            m.template.player["player_version"] = m.player.callFunc(m.bitmovinFunctions.GET_VERSION, invalid)
            end if
            If checkIfMetaConfigured(m.eventConfig.METAASSETID)
                m.template.video[m.eventConfig.METAASSETID] = m.player.id
'            getContentUrl()
        end if     
        else
            If checkIfMetaConfigured(m.eventConfig.METAPLAYERVERSION)
            m.template.player["player_version"] = "N/A"
            end if
            If checkIfMetaConfigured(m.eventConfig.METAPLAYERNAME)
            m.template.player["player_name"] = "N/A"
            end if
    
        end if
    
        If checkIfMetaConfigured(m.eventConfig.METASESSIONVIEWID)
            getContentUrl()
        end if
        
        If checkIfMetaConfigured(m.eventConfig.METAUSERAGENT)
            m.template.user_details["user_agent"] = m.device.GetModelDisplayName()+"-"+m.device.GetModel()+"-"+m.device.GetVersion()+"-"+m.device.GetChannelClientId()
        end if
        If checkIfMetaConfigured(m.eventConfig.METADZSDKVERSION)
            m.template.page["dz_sdk_version"]= "v2.0.0"
        end if
        If checkIfMetaConfigured(m.eventConfig.METACONNECTIONTYPE)
            m.template.network["connectionType"] = getConnectionType()
        end if

        getMetaData()
'     m.template.user_details.sessionStartTimestamp = m.sessionIdTimeKey
     return m.template
End Function

'Function to get the GeoLocation
function getGeoMeta()
If checkIfMetaConfigured(m.eventConfig.METAIP)
        m.template.user_details[m.eventConfig.METAIP] = getIpAddress()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACITY)
        m.template.geo_location[m.eventConfig.METACITY] = getCity()
    end if
    
    If checkIfMetaConfigured(m.eventConfig.METALONGITUDE)
        m.template.geo_location[m.eventConfig.METALONGITUDE] = getLongitude()
    end if
    If checkIfMetaConfigured(m.eventConfig.METALATITUDE)
        m.template.geo_location[m.eventConfig.METALATITUDE] = getLatitude()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAZIP)
        m.template.geo_location[m.eventConfig.METAZIP] = getZip()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACOUNTRYCODE)
        m.template.geo_location[m.eventConfig.METACOUNTRYCODE] = getCountryCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACOUNTRY)
        m.template.geo_location[m.eventConfig.METACOUNTRY] = getCountry()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREGIONCODE)
        m.template.geo_location[m.eventConfig.METAREGIONCODE] = getRegionCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREGION)
        m.template.geo_location[m.eventConfig.METAREGION] = getRegion()
    end if
    If checkIfMetaConfigured(m.eventConfig.METATIMEZONE)
        m.template.geo_location[m.eventConfig.METATIMEZONE] = getTimezone()
    end if
    If checkIfMetaConfigured(m.eventConfig.METATIMEZONEOFFSET)
        m.template.geo_location[m.eventConfig.METATIMEZONEOFFSET] = getTimezoneOffset()/3600
    end if
    If checkIfMetaConfigured(m.eventConfig.METACONTINENT)
        m.template.geo_location[m.eventConfig.METACONTINENT] = getContinent()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACONTINENTCODE)
        m.template.geo_location[m.eventConfig.METACONTINENTCODE] = getContinentCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADISTRICT)
        m.template.geo_location[m.eventConfig.METADISTRICT] = getDistrict()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAOS)
        m.template.device[m.eventConfig.METAOS] = getOS()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICETYPE)
        m.template.device[m.eventConfig.METADEVICETYPE] = getDeviceType()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICEID)
        m.template.device[m.eventConfig.METADEVICEID] = getUniqueDeviceId()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAADVERTISINGID)
        adId = getAdId()
        m.template.device[m.eventConfig.METAADVERTISINGID] = getAdId()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICENAME)
        m.template.device[m.eventConfig.METADEVICENAME] = getModelDisplayName()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICEMFG)
        m.template.device[m.eventConfig.METADEVICEMFG] = "Roku, Inc."
    end if
    If checkIfMetaConfigured(m.eventConfig.METAVIDEOTYPE)
       m.template.video[m.eventConfig.METAVIDEOTYPE] = m.videoType
    end if
    If checkIfMetaConfigured(m.eventConfig.METAOSVERSION)
        m.template.device[m.eventConfig.METAOSVERSION] = getOSVersion()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAASN)
        m.template.network[m.eventConfig.METAASN] = getasn()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAASNORG)
        m.template.network[m.eventConfig.METAASNORG] = getasnOrg()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAISP)
        m.template.network[m.eventConfig.METAISP] = getISP()
    end if
end function
'   Function to get MetaData
function getMetaData()
    if m.player <> invalid and m.player.focusedChild <> invalid
    If checkIfMetaConfigured(m.eventConfig.METADURATION)
        playerDuration = 0
        if m.player <> invalid
            if m.player.focusedChild.duration <> invalid
                playerDuration = m.player.focusedChild.duration
            end if
        end if
        m.template.video[m.eventConfig.METADURATION] = playerDuration
    end if

    If checkIfMetaConfigured(m.eventConfig.METAIP)
        m.template.user_details[m.eventConfig.METAIP] = getIpAddress()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACITY)
        m.template.geo_location[m.eventConfig.METACITY] = getCity()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAZIP)
        m.template.geo_location[m.eventConfig.METAZIP] = getZip()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACOUNTRYCODE)
        m.template.geo_location[m.eventConfig.METACOUNTRYCODE] = getCountryCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACOUNTRY)
        m.template.geo_location[m.eventConfig.METACOUNTRY] = getCountry()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREGIONCODE)
        m.template.geo_location[m.eventConfig.METAREGIONCODE] = getRegionCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREGION)
        m.template.geo_location[m.eventConfig.METAREGION] = getRegion()
    end if
        If checkIfMetaConfigured(m.eventConfig.METAOS)
        m.template.device[m.eventConfig.METAOS] = getOS()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICETYPE)
        m.template.device[m.eventConfig.METADEVICETYPE] = getDeviceType()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICEID)
        m.template.device[m.eventConfig.METADEVICEID] = getUniqueDeviceId()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAADVERTISINGID)
        adId = getAdId()
        m.template.device[m.eventConfig.METAADVERTISINGID] = getAdId()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICENAME)
        m.template.device[m.eventConfig.METADEVICENAME] = getModelDisplayName()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICEMFG)
        m.template.device[m.eventConfig.METADEVICEMFG] = "Roku, Inc."
    end if
    If checkIfMetaConfigured(m.eventConfig.METAVIDEOTYPE)
       m.template.video[m.eventConfig.METAVIDEOTYPE] = m.videoType
    end if
    If checkIfMetaConfigured(m.eventConfig.METAOSVERSION)
        m.template.device[m.eventConfig.METAOSVERSION] = getOSVersion()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAASN)
        m.template.network[m.eventConfig.METAASN] = getasn()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAASNORG)
        m.template.network[m.eventConfig.METAASNORG] = getasnOrg()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAISP)
        m.template.network[m.eventConfig.METAISP] = getISP()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACONTROLS)
         metacontrol = false
        if m.player <> invalid
            if m.player.control <> invalid
                if m.player.control <> ""
                    metacontrol = true
                end if
            end if
        end if
        m.template.player[m.eventConfig.METACONTROLS] = metacontrol
    end if
    If checkIfMetaConfigured(m.eventConfig.METALOOP)
        metaloop = false
        if m.player <> invalid
            if m.player.loop <> invalid
                if m.player.loop
                    metaloop = true
                end if
            end if
        end if
        m.template.player[m.eventConfig.METALOOP] = metaloop
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREADYSTATE)
        m.template.player[m.eventConfig.METAREADYSTATE] = getPlayerReadyState()
    end if
    If checkIfMetaConfigured(m.eventConfig.METASESSIONVIEWID)
        m.template.user_details[m.eventConfig.METASESSIONVIEWID] = getSessionViewId()
    end if
'    If checkIfMetaConfigured(m.eventConfig.METASESSIONSTARTTIMESTAMP)
'        m.template.user_details[m.eventConfig.METASESSIONSTARTTIMESTAMP] = m.sessionStartTimestamp
'    end if
    If checkIfMetaConfigured(m.eventConfig.METAVIEWID)
        m.template.user_details[m.eventConfig.METAVIEWID] = m.contentSessionId
    end if
    If checkIfMetaConfigured(m.eventConfig.METALONGITUDE)
        m.template.geo_location[m.eventConfig.METALONGITUDE] = getLongitude()
    end if
    If checkIfMetaConfigured(m.eventConfig.METALATITUDE)
        m.template.geo_location[m.eventConfig.METALATITUDE] = getLatitude()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADESCRIPTION)
        metadescription = ""
        if m.player <> invalid
            if m.player.content.description <> invalid
               metadescription = m.player.content.description
            end if
        end if
        m.template.video[m.eventConfig.METADESCRIPTION] = metadescription
    end if
    If checkIfMetaConfigured(m.eventConfig.METATITLE)
        m.metatitle = ""
        if m.videoType = "ad" and m.adTitle <> invalid
        m.metatitle = m.adTitle
        end if
        if m.player <> invalid and m.videoType = "content"
               m.metatitle = m.cnf.source.title
            if m.player.downloadFinished <> invalid
               metaurl = m.player.downloadFinished.url
            end if
        end if
        m.template.video[m.eventConfig.METATITLE] = m.metatitle
    end if
        If checkIfMetaConfigured(m.eventConfig.METASOURCE)
        m.metatitle = ""
        if m.videoType = "ad" and m.adUrl <> invalid
        metaurl = m.adUrl
        end if
        if m.player <> invalid and m.videoType = "content"
            if m.player.downloadFinished <> invalid
               metaurl = m.player.downloadFinished.url
               m.template.video[m.eventConfig.METASOURCE] = metaurl
            end if
'            if m.playerConfig<> invalid
'              ? "DZ-PRINT: SOURCE URL!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"; m.playerConfig
'               metaurl = "TEMP"
'            end if
        end if
        
    end if
        If checkIfMetaConfigured(m.eventConfig.METASTREAMINGPROTOCOL)
        m.template.player[m.eventConfig.METASTREAMINGPROTOCOL] = m.player.focusedChild.videoFormat
    end if
    If checkIfMetaConfigured(m.eventConfig.METASTREAMINGTYPE)
        if m.player <> invalid
        if m.bitmovinFunctions.IS_LIVE <> invalid
        if m.player.callFunc(m.bitmovinFunctions.IS_LIVE, invalid)
        m.template.player[m.eventConfig.METASTREAMINGTYPE] = "Live"
        else 
        m.template.player[m.eventConfig.METASTREAMINGTYPE] = "VOD"
        end if
        end if
        end if
    end if
    If checkIfMetaConfigured(m.eventConfig.METAISMUTED)
        m.template.player[m.eventConfig.METAISMUTED] = m.mute
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEFAULTMUTED)
        m.template.player[m.eventConfig.METADEFAULTMUTED] = m.defaultMute
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEFAULTPLAYBACKRATE)
        m.template.player[m.eventConfig.METADEFAULTPLAYBACKRATE] = m.defaultRate
    end if
        If checkIfMetaConfigured(m.eventConfig.METAPLAYERHEIGHT)
        m.template.player[m.eventConfig.METAPLAYERHEIGHT] = m.playerHeight
    end if
          If checkIfMetaConfigured(m.eventConfig.METAPLAYERWIDTH)
        m.template.player[m.eventConfig.METAPLAYERWIDTH] = m.playerWidth
    end if
    If checkIfMetaConfigured(m.eventConfig.METATITLE)
        
        m.metatitle = ""
        if m.videoType = "ad" and m.adTitle <> invalid
        m.metatitle = m.adTitle
        end if
        if m.player <> invalid
            if m.cnf <> invalid and m.videoType = "content"
               m.metatitle = m.cnf.source.title
            end if
            if m.player.downloadFinished <> invalid
               metaurl = m.player.downloadFinished.url
            end if
        end if
        m.template.video[m.eventConfig.METATITLE] = m.metatitle
    end if
    
    If checkIfMetaConfigured(m.eventConfig.METAFRAMERATE)
        if m.player.FrameRate <> invalid
        m.template.video[m.eventConfig.METAFRAMERATE] = m.player.FrameRate
        else 
        m.template.video[m.eventConfig.METAFRAMERATE] = 30
        end if
    end if
        
    End if
        
end function

' Function to get video URL/set url in base
function getContentUrl()
    if m.player <> invalid
       if m.player.downloadFinished <> invalid
            if  m.player.downloadFinished.url <> invalid
                setContentUrlToBase(m.player.downloadFinished.url)
                 If checkIfMetaConfigured(m.eventConfig.METAASSETID)
'                    m.template.video[m.eventConfig.METAASSETID] = m.device.GetRandomUUID()
                 end if
                return m.player.downloadFinished.url
            else
                return ""
            end if
        else
            return ""
        end if
    end if
end function

     